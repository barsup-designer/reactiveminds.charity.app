namespace ReactiveMinds.Charity.Custom
{
    using Bars.B4;
    using Bars.B4.Utils;

    /// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public partial class ResourceManifest
    {
        /// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name="container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {

        }

        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");

            container.Add(webPath, "ReactiveMinds.Charity.Custom.dll/ReactiveMinds.Charity.Custom.{0}".FormatUsing(resourceName));
        }
    }
}