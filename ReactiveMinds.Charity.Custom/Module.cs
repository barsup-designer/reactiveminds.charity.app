namespace ReactiveMinds.Charity.Custom
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Bars.B4.DataAccess;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.B4.IoC;
    using Bars.B4.Registrar;
    using Bars.B4.ResourceBundling;



    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [Bars.B4.Utils.Display("Кастомная часть для модуля Charity")]
    [Bars.B4.Utils.Description("Кастомная часть для модуля Charity")]
    [Bars.B4.Utils.CustomValue("Version", "2.2017.0415.3")]
    [Bars.B4.Utils.CustomValue("Uid", "2c6fd114-1215-0c38-47bb-fe275813b17c")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {

            Component
                    .For<Bars.B4.IResourceManifest>()
                    .LifestyleTransient()
                    .ImplementedBy<ReactiveMinds.Charity.Custom.ResourceManifest>()
            .RegisterIn(Container);




            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
            RegisterExternalResources();
        }

        protected override void SetPredecessors()
        {
            base.SetPredecessors();
            SetPredecessor<ReactiveMinds.Charity.Module>();

        }
    }
}