using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Bars.B4.Utils;

[assembly: AssemblyTitle("ReactiveMinds.Charity.Custom")]
[assembly: AssemblyDescription("B4 RMS-generated assembly for module Charity.Custom. Кастомная часть для модуля Charity")]
[assembly: AssemblyCompany("ЗАО \"БАРС Груп\"")]
[assembly: AssemblyProduct("ReactiveMinds.Charity.Custom")]
[assembly: AssemblyCopyright("Copyright © 2017")]

[assembly: ComVisible(false)]

// формат версии ВерсияГенератора.Год.МесяцДень.КоличествоКомитов
[assembly: AssemblyVersion("2.2017.0415.3")]