namespace ReactiveMinds.Charity
{
    using Bars.B4;
    using Bars.B4.Utils;

    /// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public partial class ResourceManifest
    {
        /// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name="container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {

            AddResource(container, "assets\\Types.json");
            AddResource(container, "content\\css\\app-icons.css");
            AddResource(container, "libs\\B4\\controller\\AccountEditor.js");
            AddResource(container, "libs\\B4\\controller\\AccountList.js");
            AddResource(container, "libs\\B4\\controller\\BankEditor.js");
            AddResource(container, "libs\\B4\\controller\\BankList.js");
            AddResource(container, "libs\\B4\\controller\\ClinicEditor.js");
            AddResource(container, "libs\\B4\\controller\\ClinicList.js");
            AddResource(container, "libs\\B4\\controller\\CurrencyEditor.js");
            AddResource(container, "libs\\B4\\controller\\CurrencyList.js");
            AddResource(container, "libs\\B4\\controller\\CurrencySelectionList.js");
            AddResource(container, "libs\\B4\\controller\\FundRaiseCreateEditor.js");
            AddResource(container, "libs\\B4\\controller\\FundRaiseEditor.js");
            AddResource(container, "libs\\B4\\controller\\FundRaiseList.js");
            AddResource(container, "libs\\B4\\controller\\FundRaiseTransactionEditor.js");
            AddResource(container, "libs\\B4\\controller\\FundRaiseTransactionList.js");
            AddResource(container, "libs\\B4\\controller\\FundRaiseTransactionsEditor.js");
            AddResource(container, "libs\\B4\\controller\\FundRaiseTreeEditor.js");
            AddResource(container, "libs\\B4\\controller\\PaymentSystemEditor.js");
            AddResource(container, "libs\\B4\\controller\\PaymentSystemList.js");
            AddResource(container, "libs\\B4\\controller\\TelecomEditor.js");
            AddResource(container, "libs\\B4\\controller\\TelecomList.js");
            AddResource(container, "libs\\B4\\controller\\TransactionEditor.js");
            AddResource(container, "libs\\B4\\controller\\WalletEditor.js");
            AddResource(container, "libs\\B4\\controller\\WalletExchangeTransactionEditor.js");
            AddResource(container, "libs\\B4\\controller\\WalletFundsRaiseTransactionEditor.js");
            AddResource(container, "libs\\B4\\controller\\WalletList.js");
            AddResource(container, "libs\\B4\\controller\\WalletSelectionList.js");
            AddResource(container, "libs\\B4\\CustomVTypes.js");
            AddResource(container, "libs\\B4\\view\\AccountEditor.js");
            AddResource(container, "libs\\B4\\view\\AccountList.js");
            AddResource(container, "libs\\B4\\view\\BankEditor.js");
            AddResource(container, "libs\\B4\\view\\BankList.js");
            AddResource(container, "libs\\B4\\view\\ClinicEditor.js");
            AddResource(container, "libs\\B4\\view\\ClinicList.js");
            AddResource(container, "libs\\B4\\view\\CurrencyEditor.js");
            AddResource(container, "libs\\B4\\view\\CurrencyList.js");
            AddResource(container, "libs\\B4\\view\\CurrencySelectionList.js");
            AddResource(container, "libs\\B4\\view\\FundRaiseCreateEditor.js");
            AddResource(container, "libs\\B4\\view\\FundRaiseEditor.js");
            AddResource(container, "libs\\B4\\view\\FundRaiseList.js");
            AddResource(container, "libs\\B4\\view\\FundRaiseTransactionEditor.js");
            AddResource(container, "libs\\B4\\view\\FundRaiseTransactionList.js");
            AddResource(container, "libs\\B4\\view\\FundRaiseTransactionsEditor.js");
            AddResource(container, "libs\\B4\\view\\FundRaiseTreeEditor.js");
            AddResource(container, "libs\\B4\\view\\PaymentSystemEditor.js");
            AddResource(container, "libs\\B4\\view\\PaymentSystemList.js");
            AddResource(container, "libs\\B4\\view\\TelecomEditor.js");
            AddResource(container, "libs\\B4\\view\\TelecomList.js");
            AddResource(container, "libs\\B4\\view\\TransactionEditor.js");
            AddResource(container, "libs\\B4\\view\\WalletEditor.js");
            AddResource(container, "libs\\B4\\view\\WalletExchangeTransactionEditor.js");
            AddResource(container, "libs\\B4\\view\\WalletFundsRaiseTransactionEditor.js");
            AddResource(container, "libs\\B4\\view\\WalletList.js");
            AddResource(container, "libs\\B4\\view\\WalletSelectionList.js");
        }

        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");

            container.Add(webPath, "ReactiveMinds.Charity.dll/ReactiveMinds.Charity.{0}".FormatUsing(resourceName));
        }
    }
}