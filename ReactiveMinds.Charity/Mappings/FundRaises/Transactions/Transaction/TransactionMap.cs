namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Транзакция
    /// </summary>
    public class TransactionMap : ClassMapping<ReactiveMinds.Charity.Transaction>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public TransactionMap()
        {
            Table("TRANSACTION");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            });

            #region Class: Transaction
            ManyToOne(x => x.Source, m => { m.Column("SOURCE"); m.Cascade(Cascade.Persist); });
            ManyToOne(x => x.Target, m => { m.Column("TARGET"); m.Cascade(Cascade.Persist); });
            Property(x => x.AmountInCurrency, p => { p.Column(col => { col.Name("AMOUNTINCURRENCY"); }); });
            ManyToOne(x => x.Currency, m => { m.Column("CURRENCY"); m.Cascade(Cascade.Persist); });
            Property(x => x.AmountInRubles, p => { p.Column(col => { col.Name("AMOUNTINRUBLES"); }); });
            Property(x => x.Description, p => { p.Column(col => { col.Name("DESCRIPTION"); }); });
            Property(x => x.TransactionDate, p => { p.Column(col => { col.Name("TRANSACTIONDATE"); }); });
            Property(x => x.Name, p => { p.Column(col => { col.Name("NAME"); }); });
            #endregion

            #region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p => { p.Column(col => { col.Name("OBJECT_CREATE_DATE"); }); });
            Property(x => x.ObjectEditDate, p => { p.Column(col => { col.Name("OBJECT_EDIT_DATE"); }); });
            Property(x => x.ObjectVersion, p => { p.Column(col => { col.Name("OBJECT_VERSION"); }); });
            #endregion
        }
    }

}