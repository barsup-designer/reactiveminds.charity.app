namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Транзакция сбора
    /// </summary>
    public class FundRaiseTransactionMap : JoinedSubclassMapping<ReactiveMinds.Charity.FundRaiseTransaction>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public FundRaiseTransactionMap()
        {
            Table("FUNDRAISETRANSACTION");
            Key(k => k.Column("id"));

            #region Class: FundRaiseTransaction
            ManyToOne(x => x.FundRaise, m => { m.Column("FUNDRAISE"); m.Cascade(Cascade.Persist); });
            #endregion
        }
    }

}