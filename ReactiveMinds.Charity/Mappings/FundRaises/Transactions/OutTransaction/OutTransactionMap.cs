namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Вывод средств
    /// </summary>
    public class OutTransactionMap : JoinedSubclassMapping<ReactiveMinds.Charity.OutTransaction>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public OutTransactionMap()
        {
            Table("OUTTRANSACTION");
            Key(k => k.Column("id"));

            #region Class: OutTransaction
            #endregion
        }
    }

}