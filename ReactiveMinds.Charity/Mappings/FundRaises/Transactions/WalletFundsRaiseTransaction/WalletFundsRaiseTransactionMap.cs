namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Поступление средств на кошелек
    /// </summary>
    public class WalletFundsRaiseTransactionMap : JoinedSubclassMapping<ReactiveMinds.Charity.WalletFundsRaiseTransaction>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public WalletFundsRaiseTransactionMap()
        {
            Table("WALLETFUNDSRAISETRANSACTION");
            Key(k => k.Column("id"));

            #region Class: WalletFundsRaiseTransaction
            #endregion
        }
    }

}