namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Перевод между кошельками
    /// </summary>
    public class WalletExchangeTransactionMap : JoinedSubclassMapping<ReactiveMinds.Charity.WalletExchangeTransaction>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public WalletExchangeTransactionMap()
        {
            Table("WALLETEXCHANGETRANSACTION");
            Key(k => k.Column("id"));

            #region Class: WalletExchangeTransaction
            #endregion
        }
    }

}