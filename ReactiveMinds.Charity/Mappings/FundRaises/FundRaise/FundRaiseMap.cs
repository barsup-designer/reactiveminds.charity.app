namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Сбор
    /// </summary>
    public class FundRaiseMap : ClassMapping<ReactiveMinds.Charity.FundRaise>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public FundRaiseMap()
        {
            Table("FUNDRAISE");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            });

            #region Class: FundRaise
            Property(x => x.Name, p => { p.Column(col => { col.Name("NAME"); }); });
            Property(x => x.Description, p => { p.Column(col => { col.Name("DESCRIPTION"); }); });
            #endregion
        }
    }

}