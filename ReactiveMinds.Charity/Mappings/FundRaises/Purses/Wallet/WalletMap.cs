namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Кошелек
    /// </summary>
    public class WalletMap : JoinedSubclassMapping<ReactiveMinds.Charity.Wallet>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public WalletMap()
        {
            Table("WALLET");
            Key(k => k.Column("id"));

            #region Class: Wallet
            ManyToOne(x => x.FundRaise, m => { m.Column("FUNDRAISE"); m.Cascade(Cascade.Persist); });
            ManyToOne(x => x.Account, m => { m.Column("ACCOUNT"); m.Cascade(Cascade.Persist); });
            Property(x => x.Balance, p => p.Formula("GetWalletBalanceSql(Id)"));
            #endregion
        }
    }

}