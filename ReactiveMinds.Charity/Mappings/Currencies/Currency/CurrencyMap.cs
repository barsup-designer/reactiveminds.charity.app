namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Валюта
    /// </summary>
    public class CurrencyMap : ClassMapping<ReactiveMinds.Charity.Currency>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public CurrencyMap()
        {
            Table("CURRENCY");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            });

            #region Class: Currency
            Property(x => x.ExchangeRate, p => { p.Column(col => { col.Name("EXCHANGERATE"); }); });
            Property(x => x.DisplayName, p => p.Formula("Name || \' (\' || coalesce(ExchangeRate, 0) || \' к 1)\'"));
            #endregion

            #region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p => { p.Column(col => { col.Name("OBJECT_CREATE_DATE"); }); });
            Property(x => x.ObjectEditDate, p => { p.Column(col => { col.Name("OBJECT_EDIT_DATE"); }); });
            Property(x => x.ObjectVersion, p => { p.Column(col => { col.Name("OBJECT_VERSION"); }); });
            #endregion

            #region Class: NsiEntity
            Property(x => x.Code, p => { p.Column(col => { col.Name("CODE"); }); p.NotNullable(true); });
            Property(x => x.Name, p => { p.Column(col => { col.Name("NAME"); }); p.NotNullable(true); });
            Property(x => x.MetaId, p => { p.Column(col => { col.Name("META_ID"); }); p.NotNullable(true); });
            Property(x => x.StartDate, p => { p.Column(col => { col.Name("START_DATE"); }); p.NotNullable(true); });
            Property(x => x.EndDate, p => { p.Column(col => { col.Name("END_DATE"); }); p.NotNullable(true); });
            Property(x => x.Deleted, p => { p.Column(col => { col.Name("DELETED"); }); p.NotNullable(true); });
            Property(x => x.DeletedDate, p => { p.Column(col => { col.Name("DELETED_DATE"); }); });
            ManyToOne(x => x.VersionStage, m => { m.Column("VERSION_STAGE_ID"); m.Cascade(Cascade.Persist); });
            #endregion
        }
    }

}