namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Банк
    /// </summary>
    public class BankMap : JoinedSubclassMapping<ReactiveMinds.Charity.Bank>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public BankMap()
        {
            Table("BANK");
            Key(k => k.Column("id"));

            #region Class: Bank
            #endregion
        }
    }

}