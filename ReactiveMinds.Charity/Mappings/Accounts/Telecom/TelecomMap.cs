namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Оператор связи
    /// </summary>
    public class TelecomMap : JoinedSubclassMapping<ReactiveMinds.Charity.Telecom>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public TelecomMap()
        {
            Table("TELECOM");
            Key(k => k.Column("id"));

            #region Class: Telecom
            #endregion
        }
    }

}