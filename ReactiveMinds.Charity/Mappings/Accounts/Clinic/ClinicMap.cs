namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Клиника
    /// </summary>
    public class ClinicMap : JoinedSubclassMapping<ReactiveMinds.Charity.Clinic>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ClinicMap()
        {
            Table("CLINIC");
            Key(k => k.Column("id"));

            #region Class: Clinic
            #endregion
        }
    }

}