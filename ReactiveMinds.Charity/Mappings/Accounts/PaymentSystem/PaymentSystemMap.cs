namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Платежная система
    /// </summary>
    public class PaymentSystemMap : JoinedSubclassMapping<ReactiveMinds.Charity.PaymentSystem>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public PaymentSystemMap()
        {
            Table("PAYMENTSYSTEM");
            Key(k => k.Column("id"));

            #region Class: PaymentSystem
            #endregion
        }
    }

}