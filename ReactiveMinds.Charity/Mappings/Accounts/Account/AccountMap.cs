namespace ReactiveMinds.Charity.Mappings
{
    using NHibernate.Type;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;
    using Bars.B4.DataAccess;

    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.DataAccess;

    /// <summary>
    /// Мапинг сущности Счет
    /// </summary>
    public class AccountMap : ClassMapping<ReactiveMinds.Charity.Account>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public AccountMap()
        {
            Table("ACCOUNT");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            });

            #region Class: Account
            Property(x => x.Name, p => { p.Column(col => { col.Name("NAME"); }); });
            Property(x => x.Description, p => { p.Column(col => { col.Name("DESCRIPTION"); }); });
            ManyToOne(x => x.Currency, m => { m.Column("CURRENCY"); m.Cascade(Cascade.Persist); });
            #endregion
        }
    }

}