namespace ReactiveMinds.Charity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Bars.B4.DataAccess;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.B4.IoC;
    using Bars.Rms.GeneratedApp;


    /// <summary>
    /// Модуль регистрации контроллеров
    /// </summary>    
    public partial class Module
    {
        /// <summary>
        /// Регистрация контроллеров
        /// </summary>
        protected virtual void RegisterControllers()
        {


            Container.RegisterEditorController<FundRaise, FundRaiseEditorModel>("FundRaiseEditor");
            Container.RegisterListViewController<Bank, BankListModel>("BankList");
            Container.RegisterListViewController<PaymentSystem, PaymentSystemListModel>("PaymentSystemList");
            Container.RegisterListViewController<Wallet, WalletListModel>("WalletList");
            Container.RegisterNsiListViewController<Currency, CurrencySelectionListModel>("CurrencySelectionList");
            Container.RegisterListViewController<Telecom, TelecomListModel>("TelecomList");
            Container.RegisterEditorController<FundRaise, FundRaiseCreateEditorModel>("FundRaiseCreateEditor");
            Container.RegisterListViewController<Clinic, ClinicListModel>("ClinicList");
            Container.RegisterListViewController<FundRaise, FundRaiseListModel>("FundRaiseList");
            Container.RegisterEditorController<FundRaiseTransaction, FundRaiseTransactionEditorModel>("FundRaiseTransactionEditor");
            Container.RegisterEditorController<Wallet, WalletEditorModel>("WalletEditor");
            Container.RegisterEditorController<FundRaise, FundRaiseTransactionsEditorModel>("FundRaiseTransactionsEditor");
            Container.RegisterEditorController<PaymentSystem, PaymentSystemEditorModel>("PaymentSystemEditor");
            Container.RegisterEditorController<Clinic, ClinicEditorModel>("ClinicEditor");
            Container.RegisterEditorController<WalletExchangeTransaction, WalletExchangeTransactionEditorModel>("WalletExchangeTransactionEditor");
            Container.RegisterEditorController<Telecom, TelecomEditorModel>("TelecomEditor");
            Container.RegisterEditorController<Bank, BankEditorModel>("BankEditor");
            Container.RegisterEditorController<WalletFundsRaiseTransaction, WalletFundsRaiseTransactionEditorModel>("WalletFundsRaiseTransactionEditor");
            Container.RegisterEditorController<Transaction, TransactionEditorModel>("TransactionEditor");
            Container.RegisterEditorController<FundRaise, FundRaiseTreeEditorModel>("FundRaiseTreeEditor");
            Container.RegisterNsiListViewController<Currency, CurrencyListModel>("CurrencyList");
            Container.RegisterListViewController<Wallet, WalletSelectionListModel>("WalletSelectionList");
            Container.RegisterListViewController<FundRaiseTransaction, FundRaiseTransactionListModel>("FundRaiseTransactionList");
            Container.RegisterEditorController<Account, AccountEditorModel>("AccountEditor");
            Container.RegisterEditorController<Currency, CurrencyEditorModel>("CurrencyEditor");
            Container.RegisterListViewController<Account, AccountListModel>("AccountList");


        }
    }
}