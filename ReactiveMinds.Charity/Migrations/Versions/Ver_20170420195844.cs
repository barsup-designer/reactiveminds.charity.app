namespace ReactiveMinds.Charity.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using Bars.B4.Utils;
    using Bars.B4.Modules.Ecm7.Framework;
    using Bars.B4.Modules.NH.Migrations.DatabaseExtensions;
    using Bars.Rms.GeneratedApp.Migrations;

    /// <summary>
    /// Миграция 2017.04.20.19-58-44
    /// </summary>
    [Bars.B4.Modules.Ecm7.Framework.Migration("2017.04.20.19-58-44")]
    [Bars.B4.Modules.Ecm7.Framework.MigrationDependsOn(typeof(ReactiveMinds.Charity.Migrations.Ver20170420193020))]
    public class Ver20170420195844 : BaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void Up()
        {
            EnsureDbTypes();
            DropFunctionByName("GetWalletBalanceSql");
            CreateOrReplaceFunction("GetWalletBalanceSql", "CREATE OR REPLACE FUNCTION GetWalletBalanceSql(bigint) RETURNS double precision AS $$ select \n	((select sum(coalesce(tr_in.amountinrubles, 0)) from transaction tr_in where tr_in.target = $1) -\n	(select sum(coalesce(tr_out.amountinrubles, 0)) from transaction tr_out where tr_out.source = $1))::double precision; $$ LANGUAGE \'sql\';");
            Database.ExecuteNonQuery("COMMENT ON FUNCTION GetWalletBalanceSql(bigint) is \'Получение баланса кошелька\'");
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void Down()
        {
            EnsureDbTypes();
            DropFunctionByName("GetWalletBalanceSql");
            CreateOrReplaceFunction("GetWalletBalanceSql", "CREATE OR REPLACE FUNCTION GetWalletBalanceSql(bigint) RETURNS double precision AS $$ select (sum(coalesce(in_t.amountinrubles, 0)) - sum(coalesce(out_t.amountinrubles, 0)))::double precision from WALLET w\nleft join TRANSACTION in_t on in_t.target = w.id\nleft join TRANSACTION out_t on out_t.source = w.id\nwhere w.id = $1; $$ LANGUAGE \'sql\';");
            Database.ExecuteNonQuery("COMMENT ON FUNCTION GetWalletBalanceSql(bigint) is \'Получение баланса кошелька\'");
        }
    }
}