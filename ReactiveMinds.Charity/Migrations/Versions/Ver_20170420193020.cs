namespace ReactiveMinds.Charity.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using Bars.B4.Utils;
    using Bars.B4.Modules.Ecm7.Framework;
    using Bars.B4.Modules.NH.Migrations.DatabaseExtensions;
    using Bars.Rms.GeneratedApp.Migrations;

    /// <summary>
    /// Миграция 2017.04.20.19-30-20
    /// </summary>
    [Bars.B4.Modules.Ecm7.Framework.Migration("2017.04.20.19-30-20")]
    [Bars.B4.Modules.Ecm7.Framework.MigrationDependsOn(typeof(ReactiveMinds.Charity.Migrations.Ver20170418013802))]
    public class Ver20170420193020 : BaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void Up()
        {
            EnsureDbTypes();
            DropFunctionByName("GetWalletBalanceSql");
            CreateOrReplaceFunction("GetWalletBalanceSql", "CREATE OR REPLACE FUNCTION GetWalletBalanceSql(bigint) RETURNS double precision AS $$ select (sum(coalesce(in_t.amountinrubles, 0)) - sum(coalesce(out_t.amountinrubles, 0)))::double precision from WALLET w\nleft join TRANSACTION in_t on in_t.target = w.id\nleft join TRANSACTION out_t on out_t.source = w.id\nwhere w.id = $1; $$ LANGUAGE \'sql\';");
            Database.ExecuteNonQuery("COMMENT ON FUNCTION GetWalletBalanceSql(bigint) is \'Получение баланса кошелька\'");
            Database.AddColumn("TRANSACTION", new Column("name", DbType.String.AsColumnType().WithLength(500), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN TRANSACTION.name is \'Наименование (авто)\'");
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void Down()
        {
            EnsureDbTypes();
            DropTableConstraints("TRANSACTION", "name");
            DropIndexesFromTable("TRANSACTION", "name");
            Database.RemoveColumn("TRANSACTION", "name");
            DropFunctionByName("GetWalletBalanceSql");
            CreateOrReplaceFunction("GetWalletBalanceSql", "CREATE OR REPLACE FUNCTION GetWalletBalanceSql(bigint) RETURNS double precision AS $$ select 0::double precision; $$ LANGUAGE \'sql\';");
            Database.ExecuteNonQuery("COMMENT ON FUNCTION GetWalletBalanceSql(bigint) is \'Получение баланса кошелька\'");
        }
    }
}