namespace ReactiveMinds.Charity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using System.Web.Mvc;
    using Bars.B4;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Application;
    using Bars.B4.DataAccess;
    using Bars.B4.Utils;
    using System.Linq;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Variables;

    using VariablesContainer = Bars.Rms.GeneratedApp.Variables.Container;


    /// <summary>
    /// Класс модуля. Регистрация переменных
    /// </summary>    
    public partial class Module
    {
        protected virtual void RegisterVariables()
        {




        }
    }
}