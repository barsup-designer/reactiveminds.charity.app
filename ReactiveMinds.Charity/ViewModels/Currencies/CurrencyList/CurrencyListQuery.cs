namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Castle.Windsor;
    using NHibernate.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр валют'
    /// </summary>
    public interface ICurrencyListQuery : IQueryOperation<ReactiveMinds.Charity.Currency, ReactiveMinds.Charity.CurrencyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр валют'
    /// </summary>
    public interface ICurrencyListQueryFilter : IQueryOperationFilter<ReactiveMinds.Charity.Currency, ReactiveMinds.Charity.CurrencyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр валют'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Реестр валют")]
    [ApiAccessible("Реестр валют")]
    public class CurrencyListQuery : RmsNsiQueryOperation<ReactiveMinds.Charity.Currency, ReactiveMinds.Charity.CurrencyListModel>, ICurrencyListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get { return @"[]
"; }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref="CurrencyListQuery"/>
        /// </summary>        
        /// <param name="container">Контейнер зависимостей</param>
        /// <param name="permissions">Список разрешений для выполнения запроса</param>
        public CurrencyListQuery(IWindsorContainer container, string[] permissions = null)
            : base(container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<ReactiveMinds.Charity.Currency> Filter(IQueryable<ReactiveMinds.Charity.Currency> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);

            if (@params.IsNotNull())
            {

            }

            return query;
        }


        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<ReactiveMinds.Charity.CurrencyListModel> Map(IQueryable<ReactiveMinds.Charity.Currency> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<ReactiveMinds.Charity.Currency>().GetAll();

            // формирование селектора			
            return entityQuery.Select(x => new ReactiveMinds.Charity.CurrencyListModel
            {
                Id = x.Id,
                _TypeUid = x.GetType().GUID.ToString(),
                Code = (System.String)(x.Code),
                ExchangeRate = (System.Double?)(x.ExchangeRate),
                Name = (System.String)(x.Name),
                StartDate = (System.DateTime?)(x.StartDate),
                EndDate = (System.DateTime?)(x.EndDate),
                MetaId = x.MetaId,
            });
        }
    }



}