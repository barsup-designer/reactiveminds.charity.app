namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Newtonsoft.Json;

    /// <summary>
    /// Модель записи реестра 'Реестр валют'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Реестр валют")]
    public class CurrencyListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "Bars.B4.Modules.Nsi.Entities.NsiEntity, Bars.B4.Modules.Nsi/Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_TypeUid")]
        public virtual System.String _TypeUid { get; set; }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_EditorName")]
        public virtual System.String _EditorName { get; set; }

        /// <summary>
        /// Поле 'Код' (псевдоним: Code)
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.B4.Utils.CustomValue("Uid", "ffe56b34-558b-427c-a33f-58140c18ea76")]
        public virtual System.String Code { get; set; }

        /// <summary>
        /// Поле 'Курс' (псевдоним: ExchangeRate)
        /// </summary>
        [Bars.B4.Utils.Display("Курс")]
        [Bars.B4.Utils.CustomValue("Uid", "1b130c30-1daa-4da7-8057-15d80e754871")]
        public virtual System.Double? ExchangeRate { get; set; }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: Name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "56ae526f-dffb-4bed-85c6-81ad1fce4927")]
        public virtual System.String Name { get; set; }

        /// <summary>
        /// Поле 'Дата начала' (псевдоним: StartDate)
        /// </summary>
        [Bars.B4.Utils.Display("Дата начала")]
        [Bars.B4.Utils.CustomValue("Uid", "e77fda1a-f9f2-48f8-8351-3af565e481a3")]
        public virtual System.DateTime? StartDate { get; set; }

        /// <summary>
        /// Поле 'Дата окончания' (псевдоним: EndDate)
        /// </summary>
        [Bars.B4.Utils.Display("Дата окончания")]
        [Bars.B4.Utils.CustomValue("Uid", "afaaa54d-32d4-4574-a285-2bd01a21e65c")]
        public virtual System.DateTime? EndDate { get; set; }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.B4.Utils.CustomValue("Uid", "IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf { get; set; }

        /// <summary>
        /// Мета-идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Мета-идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "MetaId")]
        public virtual System.Guid? MetaId { get; set; }

    }
}