namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Валюта'
	/// </summary>
	public interface ICurrencyEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.Currency, CurrencyEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Валюта'
	/// </summary>
    public interface ICurrencyEditorValidator : IEditorModelValidator<CurrencyEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Валюта'
    /// </summary>
    public class CurrencyEditorViewModel : BaseNsiEditorViewModel<ReactiveMinds.Charity.Currency, CurrencyEditorModel>, ICurrencyEditorViewModel
    {

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override CurrencyEditorModel CreateModel(BaseParams @params)
        {
            var model = new CurrencyEditorModel();
            model.StartDate = System.DateTime.Today;
            model.EndDate = new System.DateTime(2099, 1, 1);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Валюта' в модель представления
        /// </summary>
        public override CurrencyEditorModel MapEntity(ReactiveMinds.Charity.Currency entity)
        {
            // создаем экзепляр модели
            var model = new CurrencyEditorModel();
            model.Id = entity.Id;
            model.Code = (System.String)(entity.Code);
            model.Name = (System.String)(entity.Name);
            model.ExchangeRate = (System.Double?)(entity.ExchangeRate);
            model.StartDate = (System.DateTime?)(entity.StartDate);
            model.EndDate = (System.DateTime?)(entity.EndDate);
            model.StartDate = entity.StartDate;
            model.EndDate = entity.EndDate;
            model.MetaId = entity.MetaId;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Валюта' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.Currency entity, CurrencyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Code = model.Code;
            entity.Name = model.Name;
            if (model.ExchangeRate.HasValue)
            {
                entity.ExchangeRate = model.ExchangeRate.Value;
            }
            entity.StartDate = model.StartDate.GetValueOrDefault();
            entity.EndDate = model.EndDate.GetValueOrDefault();
            entity.MetaId = model.MetaId.GetValueOrDefault();

        }
    }
}