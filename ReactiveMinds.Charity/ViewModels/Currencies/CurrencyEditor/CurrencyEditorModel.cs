namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Валюта' для отдачи на клиент
    /// </summary>
    public class CurrencyEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public CurrencyEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Свойство 'Код'
        /// </summary>
        [Bars.B4.Utils.Display("Код")]
        [Bars.B4.Utils.CustomValue("Uid", "fac4e3d5-8a0f-4df5-b665-597f47cfc391")]
        public virtual System.String Code { get; set; }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "45ebed11-5e23-49fc-9df1-1b5029745119")]
        public virtual System.String Name { get; set; }

        /// <summary>
        /// Свойство 'Курс'
        /// </summary>
        [Bars.B4.Utils.Display("Курс")]
        [Bars.B4.Utils.CustomValue("Uid", "05189555-a2f1-4433-b20a-165f42067620")]
        public virtual System.Double? ExchangeRate { get; set; }

        /// <summary>
        /// Свойство 'Дата начала'
        /// </summary>
        [Bars.B4.Utils.Display("Дата начала")]
        [Bars.B4.Utils.CustomValue("Uid", "fd45e2ad-fcfc-4f8d-9aab-40b7fce59ab6")]
        public virtual System.DateTime? StartDate { get; set; }

        /// <summary>
        /// Свойство 'Дата окончания'
        /// </summary>
        [Bars.B4.Utils.Display("Дата окончания")]
        [Bars.B4.Utils.CustomValue("Uid", "127be122-d1ad-448d-82d0-fb6b21fd4e4f")]
        public virtual System.DateTime? EndDate { get; set; }

        /// <summary>
        /// Мета-идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Мета-идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "MetaId")]
        public virtual System.Guid? MetaId { get; set; }

    }

}