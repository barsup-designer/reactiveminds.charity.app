namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Newtonsoft.Json;

    /// <summary>
    /// Модель записи реестра 'Реестр валют.Выбор'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Реестр валют.Выбор")]
    public class CurrencySelectionListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "Bars.B4.Modules.Nsi.Entities.NsiEntity, Bars.B4.Modules.Nsi/Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_TypeUid")]
        public virtual System.String _TypeUid { get; set; }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_EditorName")]
        public virtual System.String _EditorName { get; set; }

        /// <summary>
        /// Поле 'Отображаемое имя' (псевдоним: DisplayName)
        /// </summary>
        [Bars.B4.Utils.Display("Отображаемое имя")]
        [Bars.B4.Utils.CustomValue("Uid", "50268d78-9572-458e-8fb3-9c1ab9c444f3")]
        public virtual System.String DisplayName { get; set; }

        /// <summary>
        /// Поле 'Курс' (псевдоним: ExchangeRate)
        /// </summary>
        [Bars.B4.Utils.Display("Курс")]
        [Bars.B4.Utils.CustomValue("Uid", "2f3f33b8-599d-4491-a842-c738c73d90cc")]
        public virtual System.Double? ExchangeRate { get; set; }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.B4.Utils.CustomValue("Uid", "IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf { get; set; }

        /// <summary>
        /// Мета-идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Мета-идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "MetaId")]
        public virtual System.Guid? MetaId { get; set; }

    }
}