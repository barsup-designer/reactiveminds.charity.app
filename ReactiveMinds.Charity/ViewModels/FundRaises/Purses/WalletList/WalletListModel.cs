namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Newtonsoft.Json;

    /// <summary>
    /// Модель записи реестра 'Кошельки'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Кошельки")]
    public class WalletListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_TypeUid")]
        public virtual System.String _TypeUid { get; set; }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_EditorName")]
        public virtual System.String _EditorName { get; set; }

        /// <summary>
        /// Поле 'Баланс' (псевдоним: Balance)
        /// </summary>
        [Bars.B4.Utils.Display("Баланс")]
        [Bars.B4.Utils.CustomValue("Uid", "3e3b3b84-3495-48b2-a443-6b577a8e48f6")]
        public virtual System.Double? Balance { get; set; }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: Name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "81699f46-0794-4c7d-9129-329221fc2199")]
        public virtual System.String Name { get; set; }

        /// <summary>
        /// Поле 'Сбор.Идентификатор' (псевдоним: FundRaise_Id)
        /// </summary>
        [Bars.B4.Utils.Display("Сбор.Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "d5a747e6-0bf9-491d-bad2-56208aa31c6c")]
        public virtual System.Int64? FundRaise_Id { get; set; }

        /// <summary>
        /// Поле 'Валюта счета.Идентификатор' (псевдоним: Currency_Id)
        /// </summary>
        [Bars.B4.Utils.Display("Валюта счета.Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "ea53f2f9-82f3-4b95-ac3f-5c851b149c39")]
        public virtual System.Int64? Currency_Id { get; set; }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.B4.Utils.CustomValue("Uid", "IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf { get; set; }

        /// <summary>
        /// Детали строки: Поле 'Описание')
        /// </summary>
        [Bars.B4.Utils.Display("Описание")]
        [Bars.B4.Utils.CustomValue("Uid", "ece30e54-a050-6470-7b77-f17736e55a10")]
        public virtual System.String Description { get; set; }

    }
}