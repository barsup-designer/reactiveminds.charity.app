namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Castle.Windsor;
    using NHibernate.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Кошельки'
    /// </summary>
    public interface IWalletListQuery : IQueryOperation<ReactiveMinds.Charity.Wallet, ReactiveMinds.Charity.WalletListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Кошельки'
    /// </summary>
    public interface IWalletListQueryFilter : IQueryOperationFilter<ReactiveMinds.Charity.Wallet, ReactiveMinds.Charity.WalletListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Кошельки'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Кошельки")]
    [ApiAccessible("Кошельки")]
    public class WalletListQuery : RmsEntityQueryOperation<ReactiveMinds.Charity.Wallet, ReactiveMinds.Charity.WalletListModel>, IWalletListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get { return @"[]
"; }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref="WalletListQuery"/>
        /// </summary>        
        /// <param name="container">Контейнер зависимостей</param>
        /// <param name="permissions">Список разрешений для выполнения запроса</param>
        public WalletListQuery(IWindsorContainer container, string[] permissions = null)
            : base(container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<ReactiveMinds.Charity.Wallet> Filter(IQueryable<ReactiveMinds.Charity.Wallet> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);

            if (@params.IsNotNull())
            {

            }

            return query;
        }


        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<ReactiveMinds.Charity.WalletListModel> Map(IQueryable<ReactiveMinds.Charity.Wallet> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll();

            // формирование селектора			
            return entityQuery.Select(x => new ReactiveMinds.Charity.WalletListModel
            {
                Id = x.Id,
                _TypeUid = x.GetType().GUID.ToString(),
                Balance = (System.Double?)(x.Balance),
                Name = (System.String)(x.Name),
                FundRaise_Id = (System.Int64?)(x.FundRaise.Id),
                Currency_Id = (System.Int64?)(x.Currency.Id),
                Description = (System.String)(x.Description),
            });
        }
    }



}