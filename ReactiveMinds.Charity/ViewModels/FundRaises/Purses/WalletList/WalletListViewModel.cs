namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Commons;
    using Bars.Rms.GeneratedApp.Lists;

    using NHibernate.Linq;

    /// <summary>
    /// Контракт модели представления 'Кошельки'
    /// </summary>
    public interface IWalletListViewModel : IViewModel<ReactiveMinds.Charity.Wallet, WalletListModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Кошельки'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Кошельки")]
    public class WalletListViewModel : BaseViewModel<ReactiveMinds.Charity.Wallet>, IWalletListViewModel
    {
        private IWalletListQuery _query;


        /// <summary>
        /// Конструктор модели
        /// </summary>
        /// <param name="query">Запрос данных</param>    
        public WalletListViewModel(IWalletListQuery query)
        {
            _query = query;
        }


        /// <summary>
        /// Получение и упаковка в модель одной сущности
        /// </summary>
        public override IDataResult Get(IDomainService<ReactiveMinds.Charity.Wallet> domainService, BaseParams baseParams)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public IQueryable<WalletListModel> MapQuery(IQueryable<ReactiveMinds.Charity.Wallet> entityQuery, BaseParams @params = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получение и упаковка в модель списка сущностей
        /// </summary>
        public override IDataResult List(IDomainService<ReactiveMinds.Charity.Wallet> domainService, BaseParams baseParams)
        {
            var multiAddFilterValue = baseParams.Params.GetAs("multiAddFilterVal", default(long?), true);
            var multiAddFilterField = baseParams.Params.GetAs("multiAddFilterField", string.Empty, true);

            // формирование запроса на получение типизированных моделей
            var query = _query.Query(baseParams);

            // получение параметров запроса
            var loadParams = GetLoadParam(baseParams);
            // применение клиентских фильтра
            query = query.Filter2(loadParams, this.Container);


            IList<WalletListModel> map;

            var sorters = (loadParams.Order ?? Enumerable.Empty<OrderField>()).ToArray();
            var typeOrderFields = sorters.Where(x => x.Name.IsOneOf("_TypeDisplay", "_TypeUid")).ToArray();
            if (typeOrderFields.Any())
                loadParams.Order = sorters.Except(typeOrderFields).ToArray();

            int totalCount = 0;
            var summaryData = DynamicDictionary.Create();
            if (typeof(NhQueryable<>).IsAssignableFrom(query.GetType().GetGenericTypeDefinition()))
            {

                var totalCountFuture = query.ToFutureValue(x => x.Count());
                map = query.Order(loadParams).Paging(loadParams).ToFuture().ToList();
                totalCount = totalCountFuture.Value;
            }
            else
            {

                totalCount = query.Count();
                map = query.Order(loadParams).Paging(loadParams).ToList();
            }

            if (typeOrderFields.Any())
                map = map.OrderBy(x => x._TypeUid).ToList();


            // формирование результата
            return new ListDataResultWithSummary(map, totalCount, summaryData);
        }
    }
}