namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Castle.Windsor;
    using NHibernate.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Кошельки.Выбор'
    /// </summary>
    public interface IWalletSelectionListQuery : IQueryOperation<ReactiveMinds.Charity.Wallet, ReactiveMinds.Charity.WalletSelectionListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Кошельки.Выбор'
    /// </summary>
    public interface IWalletSelectionListQueryFilter : IQueryOperationFilter<ReactiveMinds.Charity.Wallet, ReactiveMinds.Charity.WalletSelectionListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Кошельки.Выбор'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Кошельки.Выбор")]
    [ApiAccessible("Кошельки.Выбор")]
    public class WalletSelectionListQuery : RmsEntityQueryOperation<ReactiveMinds.Charity.Wallet, ReactiveMinds.Charity.WalletSelectionListModel>, IWalletSelectionListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get { return @"[]
"; }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref="WalletSelectionListQuery"/>
        /// </summary>        
        /// <param name="container">Контейнер зависимостей</param>
        /// <param name="permissions">Список разрешений для выполнения запроса</param>
        public WalletSelectionListQuery(IWindsorContainer container, string[] permissions = null)
            : base(container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<ReactiveMinds.Charity.Wallet> Filter(IQueryable<ReactiveMinds.Charity.Wallet> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);

            if (@params.IsNotNull())
            {

            }

            return query;
        }


        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<ReactiveMinds.Charity.WalletSelectionListModel> Map(IQueryable<ReactiveMinds.Charity.Wallet> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll();

            // формирование селектора			
            return entityQuery.Select(x => new ReactiveMinds.Charity.WalletSelectionListModel
            {
                Id = x.Id,
                _TypeUid = x.GetType().GUID.ToString(),
                Name = (System.String)(x.Name),
                FundRaise_Id = (System.Int64?)(x.FundRaise.Id),
                Currency_Id = (System.Int64?)(x.Currency.Id),
                Description = (System.String)(x.Description),
            });
        }
    }



}