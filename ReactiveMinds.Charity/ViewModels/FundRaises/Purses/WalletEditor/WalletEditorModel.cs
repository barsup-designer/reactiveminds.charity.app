namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Кошелек' для отдачи на клиент
    /// </summary>
    public class WalletEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public WalletEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Сбор
        /// </summary>
        [Bars.B4.Utils.Display("Сбор")]
        [Bars.B4.Utils.CustomValue("Uid", "331228ac-e958-4134-bf04-c67a7b6830ba")]
        public virtual ReactiveMinds.Charity.FundRaiseListModel FundRaise { get; set; }

        /// <summary>
        /// Базовый счет
        /// </summary>
        [Bars.B4.Utils.Display("Базовый счет")]
        [Bars.B4.Utils.CustomValue("Uid", "c02ff172-d2d4-40de-bb62-de813f43c9a4")]
        public virtual ReactiveMinds.Charity.AccountListModel Account { get; set; }

        /// <summary>
        /// Свойство 'AccountEditor'
        /// </summary>
        [Bars.B4.Utils.Display("AccountEditor")]
        [Bars.B4.Utils.CustomValue("Uid", "AccountEditor")]
        public virtual AccountEditorModel AccountEditor { get; set; }

        /// <summary>
        /// Свойство 'Наименование' (заголовок формы)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        public virtual System.String Name { get; set; }

    }

}