namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Кошелек'
	/// </summary>
	public interface IWalletEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.Wallet, WalletEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Кошелек'
	/// </summary>
    public interface IWalletEditorValidator : IEditorModelValidator<WalletEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Кошелек'
    /// </summary>
    public class WalletEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.Wallet, WalletEditorModel>, IWalletEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Счет' (AccountEditor) 
        /// </summary>
        public virtual IAccountEditorViewModel AccountEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override WalletEditorModel CreateModel(BaseParams @params)
        {
            var model = new WalletEditorModel();
            var varFundRaiseId = @params.Params.GetAs<long>("FundRaise_Id", 0);
            if (varFundRaiseId > 0)
            {
                model.FundRaise = Container.Resolve<IFundRaiseListQuery>().GetById(varFundRaiseId);
            }
            var varAccountId = @params.Params.GetAs<long>("Account_Id", 0);
            if (varAccountId > 0)
            {
                model.Account = Container.Resolve<IAccountListQuery>().GetById(varAccountId);
            }
            model.AccountEditor = AccountEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Кошелек' в модель представления
        /// </summary>
        public override WalletEditorModel MapEntity(ReactiveMinds.Charity.Wallet entity)
        {
            // создаем экзепляр модели
            var model = new WalletEditorModel();
            model.Id = entity.Id;
            if (entity.FundRaise.IsNotNull())
            {
                var queryFundRaiseList = Container.Resolve<IFundRaiseListQuery>();
                model.FundRaise = queryFundRaiseList.GetById(entity.FundRaise.Id);
            }
            if (entity.Account.IsNotNull())
            {
                var queryAccountList = Container.Resolve<IAccountListQuery>();
                model.Account = queryAccountList.GetById(entity.Account.Id);
            }
            model.AccountEditor = entity == null ? AccountEditorModel.CreateModel(new BaseParams()) : AccountEditorModel.MapEntity(entity);
            model.Name = entity.Name;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Кошелек' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.Wallet entity, WalletEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.FundRaise = TryLoadEntityById<ReactiveMinds.Charity.FundRaise>(model.FundRaise?.Id);
            entity.Account = TryLoadEntityById<ReactiveMinds.Charity.Account>(model.Account?.Id);
            if (model.AccountEditor != null)
            {
                AccountEditorModel.UnmapEntity(entity, model.AccountEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}