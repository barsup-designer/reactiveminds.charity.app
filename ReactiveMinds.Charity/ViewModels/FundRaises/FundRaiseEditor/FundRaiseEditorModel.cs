namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Сбор.Изменение' для отдачи на клиент
    /// </summary>
    public class FundRaiseEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public FundRaiseEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Свойство 'FundRaiseCreateEditor'
        /// </summary>
        [Bars.B4.Utils.Display("FundRaiseCreateEditor")]
        [Bars.B4.Utils.CustomValue("Uid", "FundRaiseCreateEditor")]
        public virtual FundRaiseCreateEditorModel FundRaiseCreateEditor { get; set; }

        /// <summary>
        /// Свойство 'Наименование' (заголовок формы)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        public virtual System.String Name { get; set; }

    }

}