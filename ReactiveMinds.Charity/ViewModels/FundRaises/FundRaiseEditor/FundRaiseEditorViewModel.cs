namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Сбор.Изменение'
	/// </summary>
	public interface IFundRaiseEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Сбор.Изменение'
	/// </summary>
    public interface IFundRaiseEditorValidator : IEditorModelValidator<FundRaiseEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Сбор.Изменение'
    /// </summary>
    public class FundRaiseEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseEditorModel>, IFundRaiseEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Сбор.Создание' (FundRaiseCreateEditor) 
        /// </summary>
        public virtual IFundRaiseCreateEditorViewModel FundRaiseCreateEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override FundRaiseEditorModel CreateModel(BaseParams @params)
        {
            var model = new FundRaiseEditorModel();
            model.FundRaiseCreateEditor = FundRaiseCreateEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Сбор' в модель представления
        /// </summary>
        public override FundRaiseEditorModel MapEntity(ReactiveMinds.Charity.FundRaise entity)
        {
            // создаем экзепляр модели
            var model = new FundRaiseEditorModel();
            model.Id = entity.Id;
            model.FundRaiseCreateEditor = entity == null ? FundRaiseCreateEditorModel.CreateModel(new BaseParams()) : FundRaiseCreateEditorModel.MapEntity(entity);
            model.Name = entity.Name;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Сбор' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.FundRaise entity, FundRaiseEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.FundRaiseCreateEditor != null)
            {
                FundRaiseCreateEditorModel.UnmapEntity(entity, model.FundRaiseCreateEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}