namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Сбор.Движение'
	/// </summary>
	public interface IFundRaiseTransactionsEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseTransactionsEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Сбор.Движение'
	/// </summary>
    public interface IFundRaiseTransactionsEditorValidator : IEditorModelValidator<FundRaiseTransactionsEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Сбор.Движение'
    /// </summary>
    public class FundRaiseTransactionsEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseTransactionsEditorModel>, IFundRaiseTransactionsEditorViewModel
    {

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override FundRaiseTransactionsEditorModel CreateModel(BaseParams @params)
        {
            var model = new FundRaiseTransactionsEditorModel();

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Сбор' в модель представления
        /// </summary>
        public override FundRaiseTransactionsEditorModel MapEntity(ReactiveMinds.Charity.FundRaise entity)
        {
            // создаем экзепляр модели
            var model = new FundRaiseTransactionsEditorModel();
            model.Id = entity.Id;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Сбор' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.FundRaise entity, FundRaiseTransactionsEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {

        }
    }
}