namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Сбор.Дерево'
	/// </summary>
	public interface IFundRaiseTreeEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseTreeEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Сбор.Дерево'
	/// </summary>
    public interface IFundRaiseTreeEditorValidator : IEditorModelValidator<FundRaiseTreeEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Сбор.Дерево'
    /// </summary>
    public class FundRaiseTreeEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseTreeEditorModel>, IFundRaiseTreeEditorViewModel
    {

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override FundRaiseTreeEditorModel CreateModel(BaseParams @params)
        {
            var model = new FundRaiseTreeEditorModel();

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Сбор' в модель представления
        /// </summary>
        public override FundRaiseTreeEditorModel MapEntity(ReactiveMinds.Charity.FundRaise entity)
        {
            // создаем экзепляр модели
            var model = new FundRaiseTreeEditorModel();
            model.Id = entity.Id;
            model.Name = entity.Name;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Сбор' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.FundRaise entity, FundRaiseTreeEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {

        }
    }
}