namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Поступление средств на кошелек' для отдачи на клиент
    /// </summary>
    public class WalletFundsRaiseTransactionEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public WalletFundsRaiseTransactionEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Приемник
        /// </summary>
        [Bars.B4.Utils.Display("Приемник")]
        [Bars.B4.Utils.CustomValue("Uid", "23d66549-9764-4746-8c8e-22357c665b4b")]
        public virtual ReactiveMinds.Charity.WalletSelectionListModel Target { get; set; }

        /// <summary>
        /// Свойство 'FundRaiseTransactionEditor'
        /// </summary>
        [Bars.B4.Utils.Display("FundRaiseTransactionEditor")]
        [Bars.B4.Utils.CustomValue("Uid", "FundRaiseTransactionEditor")]
        public virtual FundRaiseTransactionEditorModel FundRaiseTransactionEditor { get; set; }

    }

}