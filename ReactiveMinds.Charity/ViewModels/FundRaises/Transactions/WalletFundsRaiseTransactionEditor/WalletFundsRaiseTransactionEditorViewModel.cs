namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Поступление средств на кошелек'
	/// </summary>
	public interface IWalletFundsRaiseTransactionEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.WalletFundsRaiseTransaction, WalletFundsRaiseTransactionEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Поступление средств на кошелек'
	/// </summary>
    public interface IWalletFundsRaiseTransactionEditorValidator : IEditorModelValidator<WalletFundsRaiseTransactionEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Поступление средств на кошелек'
    /// </summary>
    public class WalletFundsRaiseTransactionEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.WalletFundsRaiseTransaction, WalletFundsRaiseTransactionEditorModel>, IWalletFundsRaiseTransactionEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Транзакция сбора' (FundRaiseTransactionEditor) 
        /// </summary>
        public virtual IFundRaiseTransactionEditorViewModel FundRaiseTransactionEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override WalletFundsRaiseTransactionEditorModel CreateModel(BaseParams @params)
        {
            var model = new WalletFundsRaiseTransactionEditorModel();
            model.FundRaiseTransactionEditor = FundRaiseTransactionEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Поступление средств на кошелек' в модель представления
        /// </summary>
        public override WalletFundsRaiseTransactionEditorModel MapEntity(ReactiveMinds.Charity.WalletFundsRaiseTransaction entity)
        {
            // создаем экзепляр модели
            var model = new WalletFundsRaiseTransactionEditorModel();
            model.Id = entity.Id;
            if (entity.Target.IsNotNull())
            {
                var queryWalletSelectionList = Container.Resolve<IWalletSelectionListQuery>();
                model.Target = queryWalletSelectionList.GetById(entity.Target.Id);
            }
            model.FundRaiseTransactionEditor = entity == null ? FundRaiseTransactionEditorModel.CreateModel(new BaseParams()) : FundRaiseTransactionEditorModel.MapEntity(entity);


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Поступление средств на кошелек' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.WalletFundsRaiseTransaction entity, WalletFundsRaiseTransactionEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Target = TryLoadEntityById<ReactiveMinds.Charity.Account>(model.Target?.Id);
            if (model.FundRaiseTransactionEditor != null)
            {
                FundRaiseTransactionEditorModel.UnmapEntity(entity, model.FundRaiseTransactionEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}