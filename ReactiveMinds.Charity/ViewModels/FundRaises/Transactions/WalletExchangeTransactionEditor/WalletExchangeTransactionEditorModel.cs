namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Перевод между кошельками' для отдачи на клиент
    /// </summary>
    public class WalletExchangeTransactionEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public WalletExchangeTransactionEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Источник
        /// </summary>
        [Bars.B4.Utils.Display("Источник")]
        [Bars.B4.Utils.CustomValue("Uid", "735f74cd-3c46-42fc-be06-2a9aa59560be")]
        public virtual ReactiveMinds.Charity.WalletSelectionListModel Source { get; set; }

        /// <summary>
        /// Приемник
        /// </summary>
        [Bars.B4.Utils.Display("Приемник")]
        [Bars.B4.Utils.CustomValue("Uid", "d922b559-145d-4e2b-84b7-5fdc2de1ee73")]
        public virtual ReactiveMinds.Charity.WalletSelectionListModel Target { get; set; }

        /// <summary>
        /// Свойство 'FundRaiseTransactionEditor'
        /// </summary>
        [Bars.B4.Utils.Display("FundRaiseTransactionEditor")]
        [Bars.B4.Utils.CustomValue("Uid", "FundRaiseTransactionEditor")]
        public virtual FundRaiseTransactionEditorModel FundRaiseTransactionEditor { get; set; }

    }

}