namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Перевод между кошельками'
	/// </summary>
	public interface IWalletExchangeTransactionEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.WalletExchangeTransaction, WalletExchangeTransactionEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Перевод между кошельками'
	/// </summary>
    public interface IWalletExchangeTransactionEditorValidator : IEditorModelValidator<WalletExchangeTransactionEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Перевод между кошельками'
    /// </summary>
    public class WalletExchangeTransactionEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.WalletExchangeTransaction, WalletExchangeTransactionEditorModel>, IWalletExchangeTransactionEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Транзакция сбора' (FundRaiseTransactionEditor) 
        /// </summary>
        public virtual IFundRaiseTransactionEditorViewModel FundRaiseTransactionEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override WalletExchangeTransactionEditorModel CreateModel(BaseParams @params)
        {
            var model = new WalletExchangeTransactionEditorModel();
            model.FundRaiseTransactionEditor = FundRaiseTransactionEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Перевод между кошельками' в модель представления
        /// </summary>
        public override WalletExchangeTransactionEditorModel MapEntity(ReactiveMinds.Charity.WalletExchangeTransaction entity)
        {
            // создаем экзепляр модели
            var model = new WalletExchangeTransactionEditorModel();
            model.Id = entity.Id;
            if (entity.Source.IsNotNull())
            {
                var queryWalletSelectionList = Container.Resolve<IWalletSelectionListQuery>();
                model.Source = queryWalletSelectionList.GetById(entity.Source.Id);
            }
            if (entity.Target.IsNotNull())
            {
                var queryWalletSelectionList = Container.Resolve<IWalletSelectionListQuery>();
                model.Target = queryWalletSelectionList.GetById(entity.Target.Id);
            }
            model.FundRaiseTransactionEditor = entity == null ? FundRaiseTransactionEditorModel.CreateModel(new BaseParams()) : FundRaiseTransactionEditorModel.MapEntity(entity);


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Перевод между кошельками' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.WalletExchangeTransaction entity, WalletExchangeTransactionEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Source = TryLoadEntityById<ReactiveMinds.Charity.Account>(model.Source?.Id);
            entity.Target = TryLoadEntityById<ReactiveMinds.Charity.Account>(model.Target?.Id);
            if (model.FundRaiseTransactionEditor != null)
            {
                FundRaiseTransactionEditorModel.UnmapEntity(entity, model.FundRaiseTransactionEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}