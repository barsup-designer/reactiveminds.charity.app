namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Транзакция сбора'
	/// </summary>
	public interface IFundRaiseTransactionEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.FundRaiseTransaction, FundRaiseTransactionEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Транзакция сбора'
	/// </summary>
    public interface IFundRaiseTransactionEditorValidator : IEditorModelValidator<FundRaiseTransactionEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Транзакция сбора'
    /// </summary>
    public class FundRaiseTransactionEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.FundRaiseTransaction, FundRaiseTransactionEditorModel>, IFundRaiseTransactionEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Транзакция' (TransactionEditor) 
        /// </summary>
        public virtual ITransactionEditorViewModel TransactionEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override FundRaiseTransactionEditorModel CreateModel(BaseParams @params)
        {
            var model = new FundRaiseTransactionEditorModel();
            var varFundRaiseId = @params.Params.GetAs<long>("FundRaise_Id", 0);
            if (varFundRaiseId > 0)
            {
                model.FundRaise = Container.Resolve<IFundRaiseListQuery>().GetById(varFundRaiseId);
            }
            model.TransactionEditor = TransactionEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Транзакция сбора' в модель представления
        /// </summary>
        public override FundRaiseTransactionEditorModel MapEntity(ReactiveMinds.Charity.FundRaiseTransaction entity)
        {
            // создаем экзепляр модели
            var model = new FundRaiseTransactionEditorModel();
            model.Id = entity.Id;
            if (entity.FundRaise.IsNotNull())
            {
                var queryFundRaiseList = Container.Resolve<IFundRaiseListQuery>();
                model.FundRaise = queryFundRaiseList.GetById(entity.FundRaise.Id);
            }
            model.TransactionDate = (System.DateTime?)(entity.TransactionDate);
            model.TransactionEditor = entity == null ? TransactionEditorModel.CreateModel(new BaseParams()) : TransactionEditorModel.MapEntity(entity);


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Транзакция сбора' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.FundRaiseTransaction entity, FundRaiseTransactionEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.FundRaise = TryLoadEntityById<ReactiveMinds.Charity.FundRaise>(model.FundRaise?.Id);
            entity.TransactionDate = model.TransactionDate.GetValueOrDefault();
            if (model.TransactionEditor != null)
            {
                TransactionEditorModel.UnmapEntity(entity, model.TransactionEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}