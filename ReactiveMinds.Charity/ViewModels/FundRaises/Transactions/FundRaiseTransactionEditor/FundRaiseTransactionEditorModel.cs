namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Транзакция сбора' для отдачи на клиент
    /// </summary>
    public class FundRaiseTransactionEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public FundRaiseTransactionEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Сбор
        /// </summary>
        [Bars.B4.Utils.Display("Сбор")]
        [Bars.B4.Utils.CustomValue("Uid", "4b0c45b7-2d4e-4831-960a-04e25542a6bc")]
        public virtual ReactiveMinds.Charity.FundRaiseListModel FundRaise { get; set; }

        /// <summary>
        /// Свойство 'Дата'
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.B4.Utils.CustomValue("Uid", "d4e1a9b1-1a5b-41e2-bf56-2ff25eecf5d1")]
        public virtual System.DateTime? TransactionDate { get; set; }

        /// <summary>
        /// Свойство 'TransactionEditor'
        /// </summary>
        [Bars.B4.Utils.Display("TransactionEditor")]
        [Bars.B4.Utils.CustomValue("Uid", "TransactionEditor")]
        public virtual TransactionEditorModel TransactionEditor { get; set; }

    }

}