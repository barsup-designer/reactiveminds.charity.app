namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Транзакция' для отдачи на клиент
    /// </summary>
    public class TransactionEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public TransactionEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Свойство 'Сумма в валюте'
        /// </summary>
        [Bars.B4.Utils.Display("Сумма в валюте")]
        [Bars.B4.Utils.CustomValue("Uid", "d5aee8db-3ac9-45fa-85fc-969c75a07878")]
        public virtual System.Double? AmountInCurrency { get; set; }

        /// <summary>
        /// Валюта
        /// </summary>
        [Bars.B4.Utils.Display("Валюта")]
        [Bars.B4.Utils.CustomValue("Uid", "4efcd5b4-9e23-4b79-bbde-c9626c67666c")]
        public virtual ReactiveMinds.Charity.CurrencySelectionListModel Currency { get; set; }

        /// <summary>
        /// Свойство 'Сумма в рублях'
        /// </summary>
        [Bars.B4.Utils.Display("Сумма в рублях")]
        [Bars.B4.Utils.CustomValue("Uid", "2ed3e30c-7a37-443f-9f98-512903d3e33f")]
        public virtual System.Double? AmountInRubles { get; set; }

        /// <summary>
        /// Свойство 'Комментарий'
        /// </summary>
        [Bars.B4.Utils.Display("Комментарий")]
        [Bars.B4.Utils.CustomValue("Uid", "0d297cc5-1946-4fe2-88cc-179008cc2849")]
        public virtual System.String Description { get; set; }

        /// <summary>
        /// Свойство 'Наименование (авто)'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование (авто)")]
        [Bars.B4.Utils.CustomValue("Uid", "47f78267-53bf-4631-bf62-fef7f1e52155")]
        public virtual System.String Name { get; set; }

    }

}