namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Транзакция'
	/// </summary>
	public interface ITransactionEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.Transaction, TransactionEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Транзакция'
	/// </summary>
    public interface ITransactionEditorValidator : IEditorModelValidator<TransactionEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Транзакция'
    /// </summary>
    public class TransactionEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.Transaction, TransactionEditorModel>, ITransactionEditorViewModel
    {

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override TransactionEditorModel CreateModel(BaseParams @params)
        {
            var model = new TransactionEditorModel();
            model.AmountInCurrency = 0d;
            var varCurrencyId = @params.Params.GetAs<long>("Currency_Id", 0);
            if (varCurrencyId > 0)
            {
                model.Currency = Container.Resolve<ICurrencySelectionListQuery>().GetById(varCurrencyId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Транзакция' в модель представления
        /// </summary>
        public override TransactionEditorModel MapEntity(ReactiveMinds.Charity.Transaction entity)
        {
            // создаем экзепляр модели
            var model = new TransactionEditorModel();
            model.Id = entity.Id;
            model.AmountInCurrency = (System.Double?)(entity.AmountInCurrency);
            if (entity.Currency.IsNotNull())
            {
                var queryCurrencySelectionList = Container.Resolve<ICurrencySelectionListQuery>();
                model.Currency = queryCurrencySelectionList.GetById(entity.Currency.Id);
            }
            model.AmountInRubles = (System.Double?)(entity.AmountInRubles);
            model.Description = (System.String)(entity.Description);
            model.Name = (System.String)(entity.Name);


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Транзакция' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.Transaction entity, TransactionEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.AmountInCurrency.HasValue)
            {
                entity.AmountInCurrency = model.AmountInCurrency.Value;
            }
            entity.Currency = TryLoadEntityById<ReactiveMinds.Charity.Currency>(model.Currency?.Id);
            if (model.AmountInRubles.HasValue)
            {
                entity.AmountInRubles = model.AmountInRubles.Value;
            }
            entity.Description = model.Description;
            entity.Name = model.Name;

        }
    }
}