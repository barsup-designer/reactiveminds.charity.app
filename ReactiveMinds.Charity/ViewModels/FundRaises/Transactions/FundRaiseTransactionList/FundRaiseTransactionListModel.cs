namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Newtonsoft.Json;

    /// <summary>
    /// Модель записи реестра 'Транзакции сбора'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Транзакции сбора")]
    public class FundRaiseTransactionListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_TypeUid")]
        public virtual System.String _TypeUid { get; set; }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_EditorName")]
        public virtual System.String _EditorName { get; set; }

        /// <summary>
        /// Поле 'Дата' (псевдоним: TransactionDate)
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        [Bars.B4.Utils.CustomValue("Uid", "718a0abe-9f4b-4711-bf87-c819f869aea3")]
        public virtual System.DateTime? TransactionDate { get; set; }

        /// <summary>
        /// Поле 'Валюта.Наименование' (псевдоним: Currency_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Валюта.Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "81be50ef-1522-4fa8-bbd3-55ff54cd3142")]
        public virtual System.String Currency_Name { get; set; }

        /// <summary>
        /// Поле 'Сумма в валюте' (псевдоним: AmountInCurrency)
        /// </summary>
        [Bars.B4.Utils.Display("Сумма в валюте")]
        [Bars.B4.Utils.CustomValue("Uid", "57d5ff34-f9af-404b-8b91-97f1256b0bea")]
        public virtual System.Double? AmountInCurrency { get; set; }

        /// <summary>
        /// Поле 'Сумма в рублях' (псевдоним: AmountInRubles)
        /// </summary>
        [Bars.B4.Utils.Display("Сумма в рублях")]
        [Bars.B4.Utils.CustomValue("Uid", "6fccc8c2-cb07-4a46-a4cf-112208cde47b")]
        public virtual System.Double? AmountInRubles { get; set; }

        /// <summary>
        /// Поле 'Сбор.Идентификатор' (псевдоним: FundRaise_Id)
        /// </summary>
        [Bars.B4.Utils.Display("Сбор.Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "3b2edbe6-d393-43d3-95dd-7620ff4ab92e")]
        public virtual System.Int64? FundRaise_Id { get; set; }

        /// <summary>
        /// Поле 'Наименование (авто)' (псевдоним: Name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование (авто)")]
        [Bars.B4.Utils.CustomValue("Uid", "49c06aac-d8e4-46b7-8631-c192827236a4")]
        public virtual System.String Name { get; set; }

        /// <summary>
        /// Поле 'Источник.Наименование' (псевдоним: Source_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Источник.Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "5e73e2fe-e335-49ad-aae6-9d46e14a089f")]
        public virtual System.String Source_Name { get; set; }

        /// <summary>
        /// Поле 'Приемник.Наименование' (псевдоним: Target_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Приемник.Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "99353922-ef1e-4aa1-8903-e792ad1c49a1")]
        public virtual System.String Target_Name { get; set; }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.B4.Utils.CustomValue("Uid", "IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf { get; set; }

    }
}