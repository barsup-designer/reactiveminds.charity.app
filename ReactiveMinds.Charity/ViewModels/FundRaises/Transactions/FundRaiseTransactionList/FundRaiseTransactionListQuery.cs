namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Castle.Windsor;
    using NHibernate.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Транзакции сбора'
    /// </summary>
    public interface IFundRaiseTransactionListQuery : IQueryOperation<ReactiveMinds.Charity.FundRaiseTransaction, ReactiveMinds.Charity.FundRaiseTransactionListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Транзакции сбора'
    /// </summary>
    public interface IFundRaiseTransactionListQueryFilter : IQueryOperationFilter<ReactiveMinds.Charity.FundRaiseTransaction, ReactiveMinds.Charity.FundRaiseTransactionListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Транзакции сбора'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Транзакции сбора")]
    [ApiAccessible("Транзакции сбора")]
    public class FundRaiseTransactionListQuery : RmsEntityQueryOperation<ReactiveMinds.Charity.FundRaiseTransaction, ReactiveMinds.Charity.FundRaiseTransactionListModel>, IFundRaiseTransactionListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get { return @"[]
"; }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref="FundRaiseTransactionListQuery"/>
        /// </summary>        
        /// <param name="container">Контейнер зависимостей</param>
        /// <param name="permissions">Список разрешений для выполнения запроса</param>
        public FundRaiseTransactionListQuery(IWindsorContainer container, string[] permissions = null)
            : base(container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<ReactiveMinds.Charity.FundRaiseTransaction> Filter(IQueryable<ReactiveMinds.Charity.FundRaiseTransaction> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);

            if (@params.IsNotNull())
            {

            }

            return query;
        }


        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<ReactiveMinds.Charity.FundRaiseTransactionListModel> Map(IQueryable<ReactiveMinds.Charity.FundRaiseTransaction> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll();

            // формирование селектора			
            return entityQuery.Select(x => new ReactiveMinds.Charity.FundRaiseTransactionListModel
            {
                Id = x.Id,
                _TypeUid = x.GetType().GUID.ToString(),
                TransactionDate = (System.DateTime?)(x.TransactionDate),
                Currency_Name = (System.String)(x.Currency.Name),
                AmountInCurrency = (System.Double?)(x.AmountInCurrency),
                AmountInRubles = (System.Double?)(x.AmountInRubles),
                FundRaise_Id = (System.Int64?)(x.FundRaise.Id),
                Name = (System.String)(x.Name),
                Source_Name = (System.String)(x.Source.Name),
                Target_Name = (System.String)(x.Target.Name),
            });
        }
    }



}