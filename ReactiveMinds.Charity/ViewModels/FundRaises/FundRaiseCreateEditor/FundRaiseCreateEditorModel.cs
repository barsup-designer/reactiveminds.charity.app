namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Сбор.Создание' для отдачи на клиент
    /// </summary>
    public class FundRaiseCreateEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public FundRaiseCreateEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "7fe2c995-ef6d-418f-ba23-8795d292d634")]
        public virtual System.String Name { get; set; }

        /// <summary>
        /// Свойство 'Описание'
        /// </summary>
        [Bars.B4.Utils.Display("Описание")]
        [Bars.B4.Utils.CustomValue("Uid", "2f4c3b76-8d18-4114-801c-99b28351f332")]
        public virtual System.String Description { get; set; }

    }

}