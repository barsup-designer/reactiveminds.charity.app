namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Сбор.Создание'
	/// </summary>
	public interface IFundRaiseCreateEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseCreateEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Сбор.Создание'
	/// </summary>
    public interface IFundRaiseCreateEditorValidator : IEditorModelValidator<FundRaiseCreateEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Сбор.Создание'
    /// </summary>
    public class FundRaiseCreateEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseCreateEditorModel>, IFundRaiseCreateEditorViewModel
    {

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override FundRaiseCreateEditorModel CreateModel(BaseParams @params)
        {
            var model = new FundRaiseCreateEditorModel();

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Сбор' в модель представления
        /// </summary>
        public override FundRaiseCreateEditorModel MapEntity(ReactiveMinds.Charity.FundRaise entity)
        {
            // создаем экзепляр модели
            var model = new FundRaiseCreateEditorModel();
            model.Id = entity.Id;
            model.Name = (System.String)(entity.Name);
            model.Description = (System.String)(entity.Description);


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Сбор' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.FundRaise entity, FundRaiseCreateEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Name = model.Name;
            entity.Description = model.Description;

        }
    }
}