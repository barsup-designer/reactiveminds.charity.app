namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Commons;
    using Bars.Rms.GeneratedApp.Lists;

    using NHibernate.Linq;

    /// <summary>
    /// Контракт модели представления 'Сборы'
    /// </summary>
    public interface IFundRaiseListViewModel : IViewModel<ReactiveMinds.Charity.FundRaise, FundRaiseListModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Сборы'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Сборы")]
    public class FundRaiseListViewModel : BaseViewModel<ReactiveMinds.Charity.FundRaise>, IFundRaiseListViewModel
    {
        private IFundRaiseListQuery _query;


        /// <summary>
        /// Конструктор модели
        /// </summary>
        /// <param name="query">Запрос данных</param>    
        public FundRaiseListViewModel(IFundRaiseListQuery query)
        {
            _query = query;
        }


        /// <summary>
        /// Получение и упаковка в модель одной сущности
        /// </summary>
        public override IDataResult Get(IDomainService<ReactiveMinds.Charity.FundRaise> domainService, BaseParams baseParams)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public IQueryable<FundRaiseListModel> MapQuery(IQueryable<ReactiveMinds.Charity.FundRaise> entityQuery, BaseParams @params = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получение и упаковка в модель списка сущностей
        /// </summary>
        public override IDataResult List(IDomainService<ReactiveMinds.Charity.FundRaise> domainService, BaseParams baseParams)
        {
            var multiAddFilterValue = baseParams.Params.GetAs("multiAddFilterVal", default(long?), true);
            var multiAddFilterField = baseParams.Params.GetAs("multiAddFilterField", string.Empty, true);

            // формирование запроса на получение типизированных моделей
            var query = _query.Query(baseParams);

            // получение параметров запроса
            var loadParams = GetLoadParam(baseParams);
            // применение клиентских фильтра
            query = query.Filter2(loadParams, this.Container);


            IList<FundRaiseListModel> map;

            var sorters = (loadParams.Order ?? Enumerable.Empty<OrderField>()).ToArray();
            var typeOrderFields = sorters.Where(x => x.Name.IsOneOf("_TypeDisplay", "_TypeUid")).ToArray();
            if (typeOrderFields.Any())
                loadParams.Order = sorters.Except(typeOrderFields).ToArray();

            int totalCount = 0;
            var summaryData = DynamicDictionary.Create();
            if (typeof(NhQueryable<>).IsAssignableFrom(query.GetType().GetGenericTypeDefinition()))
            {

                var totalCountFuture = query.ToFutureValue(x => x.Count());
                map = query.Order(loadParams).Paging(loadParams).ToFuture().ToList();
                totalCount = totalCountFuture.Value;
            }
            else
            {

                totalCount = query.Count();
                map = query.Order(loadParams).Paging(loadParams).ToList();
            }

            if (typeOrderFields.Any())
                map = map.OrderBy(x => x._TypeUid).ToList();

            // устанавливаем контроллер редактора для каждой из сущностей
            foreach (var itm in map)
            {
                switch (itm._TypeUid)
                {
                    case "205ab413-e912-420c-b0c4-680b1b9ee4a0":
                        itm._EditorName = "FundRaiseCreateEditor";
                        break;
                }
            }

            // формирование результата
            return new ListDataResultWithSummary(map, totalCount, summaryData);
        }
    }
}