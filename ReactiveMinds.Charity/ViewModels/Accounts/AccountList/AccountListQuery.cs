namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Castle.Windsor;
    using NHibernate.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Счета'
    /// </summary>
    public interface IAccountListQuery : IQueryOperation<ReactiveMinds.Charity.Account, ReactiveMinds.Charity.AccountListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Счета'
    /// </summary>
    public interface IAccountListQueryFilter : IQueryOperationFilter<ReactiveMinds.Charity.Account, ReactiveMinds.Charity.AccountListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Счета'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Счета")]
    [ApiAccessible("Счета")]
    public class AccountListQuery : RmsEntityQueryOperation<ReactiveMinds.Charity.Account, ReactiveMinds.Charity.AccountListModel>, IAccountListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get { return @"[]
"; }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref="AccountListQuery"/>
        /// </summary>        
        /// <param name="container">Контейнер зависимостей</param>
        /// <param name="permissions">Список разрешений для выполнения запроса</param>
        public AccountListQuery(IWindsorContainer container, string[] permissions = null)
            : base(container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<ReactiveMinds.Charity.Account> Filter(IQueryable<ReactiveMinds.Charity.Account> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);

            if (@params.IsNotNull())
            {

            }

            return query;
        }


        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<ReactiveMinds.Charity.AccountListModel> Map(IQueryable<ReactiveMinds.Charity.Account> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<ReactiveMinds.Charity.Account>().GetAll();

            // формирование селектора			
            return entityQuery.Select(x => new ReactiveMinds.Charity.AccountListModel
            {
                Id = x.Id,
                _TypeUid = x.GetType().GUID.ToString(),
                Name = (System.String)(x.Name),
                Description = (System.String)(x.Description),
            });
        }
    }



}