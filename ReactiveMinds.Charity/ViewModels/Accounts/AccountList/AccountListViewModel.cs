namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Commons;
    using Bars.Rms.GeneratedApp.Lists;

    using NHibernate.Linq;

    /// <summary>
    /// Контракт модели представления 'Счета'
    /// </summary>
    public interface IAccountListViewModel : IViewModel<ReactiveMinds.Charity.Account, AccountListModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Счета'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Счета")]
    public class AccountListViewModel : BaseViewModel<ReactiveMinds.Charity.Account>, IAccountListViewModel
    {
        private IAccountListQuery _query;


        /// <summary>
        /// Конструктор модели
        /// </summary>
        /// <param name="query">Запрос данных</param>    
        public AccountListViewModel(IAccountListQuery query)
        {
            _query = query;
        }


        /// <summary>
        /// Получение и упаковка в модель одной сущности
        /// </summary>
        public override IDataResult Get(IDomainService<ReactiveMinds.Charity.Account> domainService, BaseParams baseParams)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc />
        public IQueryable<AccountListModel> MapQuery(IQueryable<ReactiveMinds.Charity.Account> entityQuery, BaseParams @params = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получение и упаковка в модель списка сущностей
        /// </summary>
        public override IDataResult List(IDomainService<ReactiveMinds.Charity.Account> domainService, BaseParams baseParams)
        {
            var multiAddFilterValue = baseParams.Params.GetAs("multiAddFilterVal", default(long?), true);
            var multiAddFilterField = baseParams.Params.GetAs("multiAddFilterField", string.Empty, true);

            // формирование запроса на получение типизированных моделей
            var query = _query.Query(baseParams);

            // получение параметров запроса
            var loadParams = GetLoadParam(baseParams);
            // применение клиентских фильтра
            query = query.Filter2(loadParams, this.Container);


            IList<AccountListModel> map;

            var sorters = (loadParams.Order ?? Enumerable.Empty<OrderField>()).ToArray();
            var typeOrderFields = sorters.Where(x => x.Name.IsOneOf("_TypeDisplay", "_TypeUid")).ToArray();
            if (typeOrderFields.Any())
                loadParams.Order = sorters.Except(typeOrderFields).ToArray();

            int totalCount = 0;
            var summaryData = DynamicDictionary.Create();
            if (typeof(NhQueryable<>).IsAssignableFrom(query.GetType().GetGenericTypeDefinition()))
            {

                var totalCountFuture = query.ToFutureValue(x => x.Count());
                map = query.Order(loadParams).Paging(loadParams).ToFuture().ToList();
                totalCount = totalCountFuture.Value;
            }
            else
            {

                totalCount = query.Count();
                map = query.Order(loadParams).Paging(loadParams).ToList();
            }

            if (typeOrderFields.Any())
                map = map.OrderBy(x => x._TypeUid).ToList();

            // устанавливаем контроллер редактора для каждой из сущностей
            foreach (var itm in map)
            {
                switch (itm._TypeUid)
                {
                    case "06382749-301e-46d6-aec4-cc42531c39de":
                        itm._EditorName = "BankEditor";
                        break;
                    case "0e591910-5a84-43b1-9e55-449b7bc7b375":
                        itm._EditorName = "TelecomEditor";
                        break;
                    case "b656601b-2660-4617-adf6-4c02178426fd":
                        itm._EditorName = "PaymentSystemEditor";
                        break;
                    case "1d6eb087-b475-4989-a961-c18640018ebd":
                        itm._EditorName = "ClinicEditor";
                        break;
                }
            }

            // формирование результата
            return new ListDataResultWithSummary(map, totalCount, summaryData);
        }
    }
}