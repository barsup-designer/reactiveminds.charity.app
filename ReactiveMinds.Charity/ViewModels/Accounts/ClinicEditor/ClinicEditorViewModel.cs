namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Клиника'
	/// </summary>
	public interface IClinicEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.Clinic, ClinicEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Клиника'
	/// </summary>
    public interface IClinicEditorValidator : IEditorModelValidator<ClinicEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Клиника'
    /// </summary>
    public class ClinicEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.Clinic, ClinicEditorModel>, IClinicEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Счет' (AccountEditor) 
        /// </summary>
        public virtual IAccountEditorViewModel AccountEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override ClinicEditorModel CreateModel(BaseParams @params)
        {
            var model = new ClinicEditorModel();
            model.AccountEditor = AccountEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Клиника' в модель представления
        /// </summary>
        public override ClinicEditorModel MapEntity(ReactiveMinds.Charity.Clinic entity)
        {
            // создаем экзепляр модели
            var model = new ClinicEditorModel();
            model.Id = entity.Id;
            model.AccountEditor = entity == null ? AccountEditorModel.CreateModel(new BaseParams()) : AccountEditorModel.MapEntity(entity);
            model.Name = entity.Name;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Клиника' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.Clinic entity, ClinicEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.AccountEditor != null)
            {
                AccountEditorModel.UnmapEntity(entity, model.AccountEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}