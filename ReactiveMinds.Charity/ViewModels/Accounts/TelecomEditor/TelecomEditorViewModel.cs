namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Оператор связи'
	/// </summary>
	public interface ITelecomEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.Telecom, TelecomEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Оператор связи'
	/// </summary>
    public interface ITelecomEditorValidator : IEditorModelValidator<TelecomEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Оператор связи'
    /// </summary>
    public class TelecomEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.Telecom, TelecomEditorModel>, ITelecomEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Счет' (AccountEditor) 
        /// </summary>
        public virtual IAccountEditorViewModel AccountEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override TelecomEditorModel CreateModel(BaseParams @params)
        {
            var model = new TelecomEditorModel();
            model.AccountEditor = AccountEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Оператор связи' в модель представления
        /// </summary>
        public override TelecomEditorModel MapEntity(ReactiveMinds.Charity.Telecom entity)
        {
            // создаем экзепляр модели
            var model = new TelecomEditorModel();
            model.Id = entity.Id;
            model.AccountEditor = entity == null ? AccountEditorModel.CreateModel(new BaseParams()) : AccountEditorModel.MapEntity(entity);
            model.Name = entity.Name;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Оператор связи' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.Telecom entity, TelecomEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.AccountEditor != null)
            {
                AccountEditorModel.UnmapEntity(entity, model.AccountEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}