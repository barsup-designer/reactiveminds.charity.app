namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Счет'
	/// </summary>
	public interface IAccountEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.Account, AccountEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Счет'
	/// </summary>
    public interface IAccountEditorValidator : IEditorModelValidator<AccountEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Счет'
    /// </summary>
    public class AccountEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.Account, AccountEditorModel>, IAccountEditorViewModel
    {

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override AccountEditorModel CreateModel(BaseParams @params)
        {
            var model = new AccountEditorModel();
            var varCurrencyId = @params.Params.GetAs<long>("Currency_Id", 0);
            if (varCurrencyId > 0)
            {
                model.Currency = Container.Resolve<ICurrencyListQuery>().GetById(varCurrencyId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Счет' в модель представления
        /// </summary>
        public override AccountEditorModel MapEntity(ReactiveMinds.Charity.Account entity)
        {
            // создаем экзепляр модели
            var model = new AccountEditorModel();
            model.Id = entity.Id;
            model.Name = (System.String)(entity.Name);
            if (entity.Currency.IsNotNull())
            {
                var queryCurrencyList = Container.Resolve<ICurrencyListQuery>();
                model.Currency = queryCurrencyList.GetById(entity.Currency.Id);
            }
            model.Description = (System.String)(entity.Description);


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Счет' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.Account entity, AccountEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Name = model.Name;
            entity.Currency = TryLoadEntityById<ReactiveMinds.Charity.Currency>(model.Currency?.Id);
            entity.Description = model.Description;

        }
    }
}