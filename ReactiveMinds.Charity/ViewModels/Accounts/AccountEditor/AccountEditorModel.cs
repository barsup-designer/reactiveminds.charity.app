namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    /// <summary>
    /// Модель редактора 'Счет' для отдачи на клиент
    /// </summary>
    public class AccountEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public AccountEditorModel()
        {
        }
        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.B4.Utils.CustomValue("Uid", "Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "9479b9e3-f544-4cf0-8a89-b4685d8e239c")]
        public virtual System.String Name { get; set; }

        /// <summary>
        /// Валюта счета
        /// </summary>
        [Bars.B4.Utils.Display("Валюта счета")]
        [Bars.B4.Utils.CustomValue("Uid", "277dd71d-810e-4eeb-9fa3-e6c38b8f9f1a")]
        public virtual ReactiveMinds.Charity.CurrencyListModel Currency { get; set; }

        /// <summary>
        /// Свойство 'Описание'
        /// </summary>
        [Bars.B4.Utils.Display("Описание")]
        [Bars.B4.Utils.CustomValue("Uid", "d1a49e36-80f6-4f14-bf5d-99925243a414")]
        public virtual System.String Description { get; set; }

    }

}