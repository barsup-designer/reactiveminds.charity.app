namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Банк'
	/// </summary>
	public interface IBankEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.Bank, BankEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Банк'
	/// </summary>
    public interface IBankEditorValidator : IEditorModelValidator<BankEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Банк'
    /// </summary>
    public class BankEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.Bank, BankEditorModel>, IBankEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Счет' (AccountEditor) 
        /// </summary>
        public virtual IAccountEditorViewModel AccountEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override BankEditorModel CreateModel(BaseParams @params)
        {
            var model = new BankEditorModel();
            model.AccountEditor = AccountEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Банк' в модель представления
        /// </summary>
        public override BankEditorModel MapEntity(ReactiveMinds.Charity.Bank entity)
        {
            // создаем экзепляр модели
            var model = new BankEditorModel();
            model.Id = entity.Id;
            model.AccountEditor = entity == null ? AccountEditorModel.CreateModel(new BaseParams()) : AccountEditorModel.MapEntity(entity);
            model.Name = entity.Name;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Банк' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.Bank entity, BankEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.AccountEditor != null)
            {
                AccountEditorModel.UnmapEntity(entity, model.AccountEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}