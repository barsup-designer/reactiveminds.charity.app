namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Editors;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    /// <summary>
	/// Контракт модели представления 'Платежная система'
	/// </summary>
	public interface IPaymentSystemEditorViewModel : IEntityEditorViewModel<ReactiveMinds.Charity.PaymentSystem, PaymentSystemEditorModel>
    {

    }

    /// <summary>
	/// Интерфейс валидатора модели редактора 'Платежная система'
	/// </summary>
    public interface IPaymentSystemEditorValidator : IEditorModelValidator<PaymentSystemEditorModel>
    {
    }

    /// <summary>
    /// Реализация модели представления 'Платежная система'
    /// </summary>
    public class PaymentSystemEditorViewModel : BaseEditorViewModel<ReactiveMinds.Charity.PaymentSystem, PaymentSystemEditorModel>, IPaymentSystemEditorViewModel
    {
        /// <summary>
        /// Модель редактора 'Счет' (AccountEditor) 
        /// </summary>
        public virtual IAccountEditorViewModel AccountEditorModel { get; set; }

        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        public override PaymentSystemEditorModel CreateModel(BaseParams @params)
        {
            var model = new PaymentSystemEditorModel();
            model.AccountEditor = AccountEditorModel.CreateModel(@params);

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Платежная система' в модель представления
        /// </summary>
        public override PaymentSystemEditorModel MapEntity(ReactiveMinds.Charity.PaymentSystem entity)
        {
            // создаем экзепляр модели
            var model = new PaymentSystemEditorModel();
            model.Id = entity.Id;
            model.AccountEditor = entity == null ? AccountEditorModel.CreateModel(new BaseParams()) : AccountEditorModel.MapEntity(entity);
            model.Name = entity.Name;


            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Платежная система' из модели представления		
        /// </summary>
        public override void UnmapEntity(ReactiveMinds.Charity.PaymentSystem entity, PaymentSystemEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            if (model.AccountEditor != null)
            {
                AccountEditorModel.UnmapEntity(entity, model.AccountEditor, requestFiles, filesToDelete, true);
            }

        }
    }
}