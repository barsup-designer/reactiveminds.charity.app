namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Castle.Windsor;
    using NHibernate.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Банки'
    /// </summary>
    public interface IBankListQuery : IQueryOperation<ReactiveMinds.Charity.Bank, ReactiveMinds.Charity.BankListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Банки'
    /// </summary>
    public interface IBankListQueryFilter : IQueryOperationFilter<ReactiveMinds.Charity.Bank, ReactiveMinds.Charity.BankListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Банки'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Банки")]
    [ApiAccessible("Банки")]
    public class BankListQuery : RmsEntityQueryOperation<ReactiveMinds.Charity.Bank, ReactiveMinds.Charity.BankListModel>, IBankListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get { return @"[]
"; }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref="BankListQuery"/>
        /// </summary>        
        /// <param name="container">Контейнер зависимостей</param>
        /// <param name="permissions">Список разрешений для выполнения запроса</param>
        public BankListQuery(IWindsorContainer container, string[] permissions = null)
            : base(container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<ReactiveMinds.Charity.Bank> Filter(IQueryable<ReactiveMinds.Charity.Bank> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);

            if (@params.IsNotNull())
            {

            }

            return query;
        }


        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<ReactiveMinds.Charity.BankListModel> Map(IQueryable<ReactiveMinds.Charity.Bank> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<ReactiveMinds.Charity.Bank>().GetAll();

            // формирование селектора			
            return entityQuery.Select(x => new ReactiveMinds.Charity.BankListModel
            {
                Id = x.Id,
                _TypeUid = x.GetType().GUID.ToString(),
                Name = (System.String)(x.Name),
            });
        }
    }



}