namespace ReactiveMinds.Charity
{
    using System;
    using System.Linq;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Newtonsoft.Json;

    /// <summary>
    /// Модель записи реестра 'Операторы связи'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Операторы связи")]
    public class TelecomListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.B4.Utils.CustomValue("Uid", "Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id { get; set; }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_TypeUid")]
        public virtual System.String _TypeUid { get; set; }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.B4.Utils.CustomValue("Uid", "_EditorName")]
        public virtual System.String _EditorName { get; set; }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: Name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.B4.Utils.CustomValue("Uid", "fa76f991-2487-4bcd-b28f-e85043eda626")]
        public virtual System.String Name { get; set; }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.B4.Utils.CustomValue("Uid", "IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf { get; set; }

    }
}