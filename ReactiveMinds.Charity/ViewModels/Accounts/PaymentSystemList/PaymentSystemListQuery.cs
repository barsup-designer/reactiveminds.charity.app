namespace ReactiveMinds.Charity
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Diagnostics;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;
    using Bars.B4.Utils;

    using Castle.Windsor;
    using NHibernate.Linq;

    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Платежные системы'
    /// </summary>
    public interface IPaymentSystemListQuery : IQueryOperation<ReactiveMinds.Charity.PaymentSystem, ReactiveMinds.Charity.PaymentSystemListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Платежные системы'
    /// </summary>
    public interface IPaymentSystemListQueryFilter : IQueryOperationFilter<ReactiveMinds.Charity.PaymentSystem, ReactiveMinds.Charity.PaymentSystemListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Платежные системы'
    /// </summary>
	[Bars.B4.Utils.DisplayAttribute("Платежные системы")]
    [ApiAccessible("Платежные системы")]
    public class PaymentSystemListQuery : RmsEntityQueryOperation<ReactiveMinds.Charity.PaymentSystem, ReactiveMinds.Charity.PaymentSystemListModel>, IPaymentSystemListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get { return @"[]
"; }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref="PaymentSystemListQuery"/>
        /// </summary>        
        /// <param name="container">Контейнер зависимостей</param>
        /// <param name="permissions">Список разрешений для выполнения запроса</param>
        public PaymentSystemListQuery(IWindsorContainer container, string[] permissions = null)
            : base(container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name="entityQuery">Запрос сущностей</param>
        /// <param name="params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<ReactiveMinds.Charity.PaymentSystem> Filter(IQueryable<ReactiveMinds.Charity.PaymentSystem> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);

            if (@params.IsNotNull())
            {

            }

            return query;
        }


        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<ReactiveMinds.Charity.PaymentSystemListModel> Map(IQueryable<ReactiveMinds.Charity.PaymentSystem> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<ReactiveMinds.Charity.PaymentSystem>().GetAll();

            // формирование селектора			
            return entityQuery.Select(x => new ReactiveMinds.Charity.PaymentSystemListModel
            {
                Id = x.Id,
                _TypeUid = x.GetType().GUID.ToString(),
                Name = (System.String)(x.Name),
            });
        }
    }



}