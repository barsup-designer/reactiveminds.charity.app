namespace ReactiveMinds.Charity
{
    using Bars.B4;
    using Bars.B4.Modules.ExtJs;
    using System.Collections.Generic;
    using System.Linq;
    using Bars.B4.Utils;
    using Bars.B4.Modules.Security;

    public partial class ResourceManifest : ResourceManifestBase
    {
        protected override void AdditionalInit(IResourceManifestContainer container)
        {
            #region Перечисления



            #endregion

            #region Модели

            container.RegisterExtJsModel<ReactiveMinds.Charity.FundRaiseEditorModel>().ActionMethods(read: "POST").Controller("FundRaiseEditor");
            container.RegisterExtJsModel<BankListModel>().ActionMethods(read: "POST").Controller("BankList");
            container.RegisterExtJsModel<PaymentSystemListModel>().ActionMethods(read: "POST").Controller("PaymentSystemList");
            container.RegisterExtJsModel<WalletListModel>().ActionMethods(read: "POST").Controller("WalletList");
            container.RegisterExtJsModel<CurrencySelectionListModel>().ActionMethods(read: "POST").Controller("CurrencySelectionList");
            container.RegisterExtJsModel<TelecomListModel>().ActionMethods(read: "POST").Controller("TelecomList");
            container.RegisterExtJsModel<ReactiveMinds.Charity.FundRaiseCreateEditorModel>().ActionMethods(read: "POST").Controller("FundRaiseCreateEditor");
            container.RegisterExtJsModel<ClinicListModel>().ActionMethods(read: "POST").Controller("ClinicList");
            container.RegisterExtJsModel<FundRaiseListModel>().ActionMethods(read: "POST").Controller("FundRaiseList");
            container.RegisterExtJsModel<ReactiveMinds.Charity.FundRaiseTransactionEditorModel>().ActionMethods(read: "POST").Controller("FundRaiseTransactionEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.WalletEditorModel>().ActionMethods(read: "POST").Controller("WalletEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.FundRaiseTransactionsEditorModel>().ActionMethods(read: "POST").Controller("FundRaiseTransactionsEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.PaymentSystemEditorModel>().ActionMethods(read: "POST").Controller("PaymentSystemEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.ClinicEditorModel>().ActionMethods(read: "POST").Controller("ClinicEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.WalletExchangeTransactionEditorModel>().ActionMethods(read: "POST").Controller("WalletExchangeTransactionEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.TelecomEditorModel>().ActionMethods(read: "POST").Controller("TelecomEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.BankEditorModel>().ActionMethods(read: "POST").Controller("BankEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.WalletFundsRaiseTransactionEditorModel>().ActionMethods(read: "POST").Controller("WalletFundsRaiseTransactionEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.TransactionEditorModel>().ActionMethods(read: "POST").Controller("TransactionEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.FundRaiseTreeEditorModel>().ActionMethods(read: "POST").Controller("FundRaiseTreeEditor");
            container.RegisterExtJsModel<CurrencyListModel>().ActionMethods(read: "POST").Controller("CurrencyList");
            container.RegisterExtJsModel<WalletSelectionListModel>().ActionMethods(read: "POST").Controller("WalletSelectionList");
            container.RegisterExtJsModel<FundRaiseTransactionListModel>().ActionMethods(read: "POST").Controller("FundRaiseTransactionList");
            container.RegisterExtJsModel<ReactiveMinds.Charity.AccountEditorModel>().ActionMethods(read: "POST").Controller("AccountEditor");
            container.RegisterExtJsModel<ReactiveMinds.Charity.CurrencyEditorModel>().ActionMethods(read: "POST").Controller("CurrencyEditor");
            container.RegisterExtJsModel<AccountListModel>().ActionMethods(read: "POST").Controller("AccountList");
            container.RegisterExtJsModel<Bars.B4.Modules.FileStorage.FileInfo>().ActionMethods(read: "POST").Controller("Empty");



            #endregion
        }
    }
}