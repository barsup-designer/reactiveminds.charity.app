namespace ReactiveMinds.Charity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Bars.B4.DataAccess;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.B4.IoC;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.States;


    public partial class Module
    {
        protected virtual void RegisterDomainServices()
        {
            Container.RegisterViewModel<ReactiveMinds.Charity.CurrencyListViewModel>();
            Component.For<Bars.B4.Modules.NHibernateChangeLog.IAuditLogMapProvider>().ImplementedBy<ReactiveMinds.Charity.AuditLogMapProvider>().LifestyleSingleton().RegisterIn(Container);
            Container.RegisterQueryOperation<ReactiveMinds.Charity.TelecomListQuery>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.CurrencySelectionListQuery>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.TelecomEditorViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.TelecomListViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.FundRaiseCreateEditorViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.ClinicEditorViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.TransactionEditorViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.PaymentSystemEditorViewModel>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.AccountListQuery>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.CurrencyListQuery>();
            Container.RegisterViewModel<ReactiveMinds.Charity.BankListViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.FundRaiseTransactionListViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.FundRaiseTransactionEditorViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.WalletSelectionListViewModel>();
            Component
.For<Bars.B4.Modules.States.IStatefulEntitiesManifest>()
.ImplementedBy<ReactiveMinds.Charity.States.StatesManifest>()
.LifestyleTransient()
.RegisterIn(Container);
            Container.RegisterQueryOperation<ReactiveMinds.Charity.FundRaiseTransactionListQuery>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.WalletExchangeTransactionEditorViewModel>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.BankListQuery>();
            Container.RegisterNsiDomainService<ReactiveMinds.Charity.Currency>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.FundRaiseEditorViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.FundRaiseTransactionsEditorViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.CurrencySelectionListViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.FundRaiseListViewModel>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.PaymentSystemListQuery>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.WalletEditorViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.WalletListViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.ClinicListViewModel>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.WalletListQuery>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.FundRaiseTreeEditorViewModel>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.ClinicListQuery>();
            Container.RegisterQueryOperation<ReactiveMinds.Charity.FundRaiseListQuery>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.WalletFundsRaiseTransactionEditorViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.AccountEditorViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.PaymentSystemListViewModel>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.BankEditorViewModel>();
            Component.For<Bars.B4.IModuleDependencies>().ImplementedBy<ReactiveMinds.Charity.ModuleDependencies>().LifestyleSingleton().RegisterIn(Container);
            Container.RegisterQueryOperation<ReactiveMinds.Charity.WalletSelectionListQuery>();
            Container.RegisterEditorViewModel<ReactiveMinds.Charity.CurrencyEditorViewModel>();
            Container.RegisterViewModel<ReactiveMinds.Charity.AccountListViewModel>();
        }
    }
}