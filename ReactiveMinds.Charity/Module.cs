namespace ReactiveMinds.Charity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Bars.B4.DataAccess;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.B4.IoC;
    using Bars.B4.Registrar;
    using Bars.B4.ResourceBundling;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [Bars.B4.Utils.Display("Благотворительность")]
    [Bars.B4.Utils.Description("")]
    [Bars.B4.Utils.CustomValue("Version", "2.2017.0418.4")]
    [Bars.B4.Utils.CustomValue("Uid", "166c9fa4-b31a-4ecd-8c82-c95007b3f7b5")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {

            Container.Resolve<Bars.B4.Modules.Nsi.DictionaryMetadata.Interfaces.Registrators.INsiDictionaryRegistrator>().RegisterDictionary<ReactiveMinds.Charity.Currency>();
            Container.RegisterResourceManifest<ReactiveMinds.Charity.ResourceManifest>();

            Container.RegisterTransient<Bars.B4.License.ILicenseInfo, LicenceInformation>();

            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
            RegisterExternalResources();
        }

        protected override void SetPredecessors()
        {
            base.SetPredecessors();
            SetPredecessor<Bars.Rms.GeneratedApp.Module>();
            SetPredecessor<Bars.B4.Modules.FIAS.Module>();
            SetPredecessor<Bars.B4.Modules.FileSystemStorage.Module>();
            SetPredecessor<Bars.B4.Modules.FlexDesk.Module>();
            SetPredecessor<Bars.B4.Modules.Mapping.NHibernate.Module>();
            SetPredecessor<Bars.B4.Modules.Nsi.Module>();
            SetPredecessor<Bars.B4.Modules.QueryDesigner.Module>();
            SetPredecessor<Bars.B4.Modules.QueryDesigner.ExtJS4.Module>();
            SetPredecessor<Bars.B4.Modules.ReportDesigner.Module>();
            SetPredecessor<Bars.B4.Modules.Security.Module>();
            SetPredecessor<Bars.B4.Modules.Security.ExtJS4.Module>();
            SetPredecessor<Bars.B4.Modules.States.Module>();
            SetPredecessor<Bars.B4.Modules.Plv8.Module>();

        }
    }
}