namespace ReactiveMinds.Charity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Castle.Windsor;
    using Castle.MicroKernel.Registration;
    using Bars.B4.DataAccess;
    using System.Web.Mvc;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.B4.IoC;
    using Bars.B4.ResourceBundling;


    public partial class Module
    {
        protected virtual void RegisterNavigationProviders()
        {
            Component.For<Bars.B4.IClientRouteMapRegistrar>().ImplementedBy<ReactiveMinds.Charity.MainMenuClientRoute>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterSessionScoped<Bars.Rms.GeneratedApp.Navigation.INavigationPanelNodesProvider, ReactiveMinds.Charity.Navigation.Providers.NavContainer3d1a9409e9f546bfa49ce22338051b00Provider>();
            Container.RegisterNavigationProvider<ReactiveMinds.Charity.MainMenuNavigation>();
            Component.For<Bars.B4.IClientRouteMapRegistrar>().ImplementedBy<ReactiveMinds.Charity.ActionClientRoute>().LifestyleTransient().RegisterIn(Container);
        }
    }
}