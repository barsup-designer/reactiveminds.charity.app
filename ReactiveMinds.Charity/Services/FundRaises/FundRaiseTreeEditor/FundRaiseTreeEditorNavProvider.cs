namespace ReactiveMinds.Charity.Navigation.Providers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Bars.B4;
    using Bars.B4.Cache;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.Security;
    using Bars.B4.Utils;
    using Castle.Windsor;
    using Bars.Rms.GeneratedApp;
    using Bars.Rms.GeneratedApp.Navigation;

    /// <summary>
    /// Поставщик узлов панели навигации
    /// </summary>
    public class NavContainer3d1a9409e9f546bfa49ce22338051b00Provider : NavigationPanelNodesProvider<ReactiveMinds.Charity.FundRaise>
    {
        /// <summary>
        /// Ключ, для которого формируются элементы
        /// </summary>
        public override string Key
        {
            get { return "3d1a9409-e9f5-46bf-a49c-e22338051b00"; }
        }
        /// <summary>
        /// Формирование списка элементов дерева
        /// </summary>
        public override void FillSructure(BaseParams @params, IList<NavigationPanelNode> structure)
        {
            var entityId = @params.Params.GetAs<long?>("EntityId");
            var entity = entityId.HasValue && entityId > 0 ? Service.Get(entityId) : null;

            //-------
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"e3454f56-334b-a3e5-8501-89e86c1b0d49-{entity.Return(v => v.Id)}")
                {
                    // устанавливаем параметры контекста
                    ContextProperties = DynamicDictionary.Create().SetValue("FundRaise_Id", entityId),
                    // устанавливаем параметры представления
                    ViewProperties = DynamicDictionary.Create(),
                    ElementUid = "dba518ae-cf42-404b-b3bc-3619c310e9e5",
                    Action = null,
                    Controller = "B4.controller.FundRaiseEditor",
                    Name = "Основное",
                    EntityId = entity.Return(v => v.Id),
                    IconCls = null,
                    Key = null,
                    SelectByDefault = false,
                    Leaf = true
                });
            }

            //-------
            if (entity.IsNotNull())
            {
                structure.Add(new NavigationPanelNode($"b8b59765-59c7-9da1-c9e5-c64130525baa-{entity.Return(v => v.Id)}")
                {
                    // устанавливаем параметры контекста
                    ContextProperties = DynamicDictionary.Create().SetValue("FundRaise_Id", entityId),
                    // устанавливаем параметры представления
                    ViewProperties = DynamicDictionary.Create(),
                    ElementUid = "e1067c06-69fb-4f0d-a3ac-ea3cbf751d2a",
                    Action = null,
                    Controller = "B4.controller.FundRaiseTransactionsEditor",
                    Name = "Движение средств",
                    EntityId = entity.Return(v => v.Id),
                    IconCls = null,
                    Key = null,
                    SelectByDefault = false,
                    Leaf = true
                });
            }
        }
    }

}