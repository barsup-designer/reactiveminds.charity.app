Ext.define('B4.controller.WalletEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'WalletEditorModel'],
    views: [
        'WalletEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.WalletEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-walleteditor',
    viewDataModel: 'WalletEditorModel',
    viewDataController: 'WalletEditor',
    viewDataName: 'WalletEditor',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            '[rmsUid=c02ff172-d2d4-40de-bb62-de813f43c9a4]': {
                'change': function(sender, newValue, oldValue) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=c02ff172-d2d4-40de-bb62-de813f43c9a4]'),
                        x = cmp.getRawValue(),
                        cmp1 = this.down('[rmsUid=9479b9e3-f544-4cf0-8a89-b4685d8e239c]');
                    cmp1.setValue(x);
                }
            }
        });
        me.callParent(arguments);
        this.initControllers(['B4.controller.AccountEditor']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Счет
        element = form.down('[rmsUid=36491c45-16c9-4109-959b-524cf36ce43d]');
        ctrl = me.getController('B4.controller.AccountEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(rec.get('AccountEditor'));
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(model, element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['FundRaise_Id'])) {
            this.preventFieldInput(form, ['[name=FundRaise]']);
        }
        if (!Ext.isEmpty(ctxParams['Account_Id'])) {
            this.preventFieldInput(form, ['[name=Account]']);
        }
        // Счет
        element = form.down('[rmsUid=36491c45-16c9-4109-959b-524cf36ce43d]');
        ctrl = me.getController('B4.controller.AccountEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(record.get('AccountEditor'));
        ctrl.onSetViewData(model, element);
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Счет
        element = view.down('[rmsUid=36491c45-16c9-4109-959b-524cf36ce43d]');
        ctrl = me.getController('B4.controller.AccountEditor');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        // Счет
        element = view.down('[rmsUid=36491c45-16c9-4109-959b-524cf36ce43d]');
        ctrl = me.getController('B4.controller.AccountEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(element.getEditorValues());
        ctrl.onViewDeployed(element, model);
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});