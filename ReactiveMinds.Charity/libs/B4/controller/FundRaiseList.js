Ext.define('B4.controller.FundRaiseList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.FundRaiseList',
        'B4.model.FundRaiseListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-fundraiselist',
    viewDataName: 'FundRaiseList',
    // набор описаний действий реестра
    actions: {
        'Addition-FundRaiseCreateEditor-InWindow': {
            'editor': 'FundRaiseCreateEditor',
            'editorAlias': 'rms-fundraisecreateeditor',
            'editorUid': 'c5500f61-b5e5-4ee8-9ebf-86c0f331bccc',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 275,
                'width': 500
            }
        },
        'Editing-FundRaiseTreeEditor-Default': {
            'editor': 'FundRaiseTreeEditor',
            'editorAlias': 'rms-fundraisetreeeditor',
            'editorUid': 'fcfb5470-7851-45c4-ae61-3c33240abc41',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'Default',
            'redirectTo': 'FundRaiseTreeEditor/Edit/{id}/'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-fundraiselist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-FundRaiseTreeEditor-Default', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-fundraiselist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});