Ext.define('B4.controller.ClinicList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.ClinicList',
        'B4.model.ClinicListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-cliniclist',
    viewDataName: 'ClinicList',
    // набор описаний действий реестра
    actions: {
        'Addition-ClinicEditor-InWindow': {
            'editor': 'ClinicEditor',
            'editorAlias': 'rms-cliniceditor',
            'editorUid': '0988d306-df07-466b-b32f-06ff85a432bf',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-ClinicEditor-InWindow': {
            'editor': 'ClinicEditor',
            'editorAlias': 'rms-cliniceditor',
            'editorUid': '0988d306-df07-466b-b32f-06ff85a432bf',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-cliniclist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-ClinicEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-cliniclist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});