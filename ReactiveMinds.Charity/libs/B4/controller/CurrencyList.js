Ext.define('B4.controller.CurrencyList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.CurrencyList',
        'B4.model.CurrencyListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-currencylist',
    viewDataName: 'CurrencyList',
    // набор описаний действий реестра
    actions: {
        'Addition-CurrencyEditor-InWindow': {
            'editor': 'CurrencyEditor',
            'editorAlias': 'rms-currencyeditor',
            'editorUid': 'dfc6489a-ff0f-487d-8839-1a9856aef807',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 200,
                'width': 500
            }
        },
        'Editing-CurrencyEditor-InWindow': {
            'editor': 'CurrencyEditor',
            'editorAlias': 'rms-currencyeditor',
            'editorUid': 'dfc6489a-ff0f-487d-8839-1a9856aef807',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 200,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-currencylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-CurrencyEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-currencylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});