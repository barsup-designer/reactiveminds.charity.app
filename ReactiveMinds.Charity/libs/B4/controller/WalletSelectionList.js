Ext.define('B4.controller.WalletSelectionList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.WalletSelectionList',
        'B4.model.WalletSelectionListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-walletselectionlist',
    viewDataName: 'WalletSelectionList',
    // набор описаний действий реестра
    actions: {
        'Addition-WalletEditor-InWindow': {
            'editor': 'WalletEditor',
            'editorAlias': 'rms-walleteditor',
            'editorUid': '8c88219d-6274-4f41-bdc8-1610a2666faf',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 459,
                'width': 500
            }
        },
        'Editing-WalletEditor-InWindow': {
            'editor': 'WalletEditor',
            'editorAlias': 'rms-walleteditor',
            'editorUid': '8c88219d-6274-4f41-bdc8-1610a2666faf',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 459,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-walletselectionlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-WalletEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-walletselectionlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['FundRaise_Id'])) {
            this.hideColumnByDataIndex('FundRaise_Id', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Currency_Id'])) {
            this.hideColumnByDataIndex('Currency_Id', true, view);
        }
    },
});