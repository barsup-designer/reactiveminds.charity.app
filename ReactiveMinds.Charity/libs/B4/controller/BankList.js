Ext.define('B4.controller.BankList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.BankList',
        'B4.model.BankListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-banklist',
    viewDataName: 'BankList',
    // набор описаний действий реестра
    actions: {
        'Addition-BankEditor-InWindow': {
            'editor': 'BankEditor',
            'editorAlias': 'rms-bankeditor',
            'editorUid': 'b0230488-b8ee-4af3-8dfb-1d2e0e20c487',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-BankEditor-InWindow': {
            'editor': 'BankEditor',
            'editorAlias': 'rms-bankeditor',
            'editorUid': 'b0230488-b8ee-4af3-8dfb-1d2e0e20c487',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-banklist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-BankEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-banklist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});