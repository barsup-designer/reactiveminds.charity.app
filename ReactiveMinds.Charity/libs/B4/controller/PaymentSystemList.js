Ext.define('B4.controller.PaymentSystemList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.PaymentSystemList',
        'B4.model.PaymentSystemListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-paymentsystemlist',
    viewDataName: 'PaymentSystemList',
    // набор описаний действий реестра
    actions: {
        'Addition-PaymentSystemEditor-InWindow': {
            'editor': 'PaymentSystemEditor',
            'editorAlias': 'rms-paymentsystemeditor',
            'editorUid': 'e91a0429-c051-40d2-983c-25bbaa71ed91',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-PaymentSystemEditor-InWindow': {
            'editor': 'PaymentSystemEditor',
            'editorAlias': 'rms-paymentsystemeditor',
            'editorUid': 'e91a0429-c051-40d2-983c-25bbaa71ed91',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-paymentsystemlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-PaymentSystemEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-paymentsystemlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});