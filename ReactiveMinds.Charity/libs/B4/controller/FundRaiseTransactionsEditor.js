Ext.define('B4.controller.FundRaiseTransactionsEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'FundRaiseTransactionsEditorModel'],
    views: [
        'FundRaiseTransactionsEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.FundRaiseTransactionsEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-fundraisetransactionseditor',
    viewDataModel: 'FundRaiseTransactionsEditorModel',
    viewDataController: 'FundRaiseTransactionsEditor',
    viewDataName: 'FundRaiseTransactionsEditor',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.FundRaiseTransactionList']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Транзакции сбора
        element = form.down('[name=FundRaiseTransactionList]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionList');
        element.data.set('FundRaise_Id', rec.get('Id') || 0);
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (isNewRecord == true) {
            form.grid_FundRaiseTransactionList.disableGrid();
        } else {
            form.grid_FundRaiseTransactionList.enable();
            form.grid_FundRaiseTransactionList.getStore().load();
        }
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Транзакции сбора
        element = view.down('[name=FundRaiseTransactionList]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionList');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});