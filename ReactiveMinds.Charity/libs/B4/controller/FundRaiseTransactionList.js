Ext.define('B4.controller.FundRaiseTransactionList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.FundRaiseTransactionList',
        'B4.model.FundRaiseTransactionListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-fundraisetransactionlist',
    viewDataName: 'FundRaiseTransactionList',
    // набор описаний действий реестра
    actions: {
        'Addition-WalletExchangeTransactionEditor-InWindow': {
            'editor': 'WalletExchangeTransactionEditor',
            'editorAlias': 'rms-walletexchangetransactioneditor',
            'editorUid': 'c64e0ad7-a884-4ce3-bf6f-fb4abd8b40a1',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 408,
                'width': 500
            }
        },
        'Addition-WalletFundsRaiseTransactionEditor-InWindow': {
            'editor': 'WalletFundsRaiseTransactionEditor',
            'editorAlias': 'rms-walletfundsraisetransactioneditor',
            'editorUid': '8bb0ed10-de66-4089-a912-5929f517c332',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 404,
                'width': 500
            }
        },
        'Editing': {
            'editor': '',
            'editorAlias': '',
            'editorUid': '',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-fundraisetransactionlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-fundraisetransactionlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['FundRaise_Id'])) {
            this.hideColumnByDataIndex('FundRaise_Id', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Source_Id'])) {
            this.hideColumnByDataIndex('Source_Name', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Target_Id'])) {
            this.hideColumnByDataIndex('Target_Name', true, view);
        }
    },
});