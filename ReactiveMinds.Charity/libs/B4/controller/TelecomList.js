Ext.define('B4.controller.TelecomList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.TelecomList',
        'B4.model.TelecomListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-telecomlist',
    viewDataName: 'TelecomList',
    // набор описаний действий реестра
    actions: {
        'Addition-TelecomEditor-InWindow': {
            'editor': 'TelecomEditor',
            'editorAlias': 'rms-telecomeditor',
            'editorUid': '4ddd1e24-cad5-42ba-94a0-520e0d7c3f19',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing-TelecomEditor-InWindow': {
            'editor': 'TelecomEditor',
            'editorAlias': 'rms-telecomeditor',
            'editorUid': '4ddd1e24-cad5-42ba-94a0-520e0d7c3f19',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-telecomlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-TelecomEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-telecomlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});