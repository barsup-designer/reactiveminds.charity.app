Ext.define('B4.controller.WalletExchangeTransactionEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'WalletExchangeTransactionEditorModel'],
    views: [
        'WalletExchangeTransactionEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.WalletExchangeTransactionEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-walletexchangetransactioneditor',
    viewDataModel: 'WalletExchangeTransactionEditorModel',
    viewDataController: 'WalletExchangeTransactionEditor',
    viewDataName: 'WalletExchangeTransactionEditor',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-walletexchangetransactioneditor': {
                'onViewDataChanged': function(view) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=2ed3e30c-7a37-443f-9f98-512903d3e33f]'),
                        cmp1 = this.down('[rmsUid=735f74cd-3c46-42fc-be06-2a9aa59560be]'),
                        cmp2 = this.down('[rmsUid=d922b559-145d-4e2b-84b7-5fdc2de1ee73]'),
                        cmp3 = this.down('[rmsUid=47f78267-53bf-4631-bf62-fef7f1e52155]');
                    cmp3.setValue(['Перевод ', cmp.getRawValue(), 'р. из ', cmp1.getRawValue(), ' в ', cmp2.getRawValue()].join(''));
                }
            }
        });
        me.callParent(arguments);
        this.initControllers(['B4.controller.FundRaiseTransactionEditor']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Транзакция сбора
        element = form.down('[rmsUid=57f27279-f224-44da-a716-010e647cb617]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(rec.get('FundRaiseTransactionEditor'));
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(model, element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        // Транзакция сбора
        element = form.down('[rmsUid=57f27279-f224-44da-a716-010e647cb617]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(record.get('FundRaiseTransactionEditor'));
        ctrl.onSetViewData(model, element);
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Транзакция сбора
        element = view.down('[rmsUid=57f27279-f224-44da-a716-010e647cb617]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        // Транзакция сбора
        element = view.down('[rmsUid=57f27279-f224-44da-a716-010e647cb617]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(element.getEditorValues());
        ctrl.onViewDeployed(element, model);
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});