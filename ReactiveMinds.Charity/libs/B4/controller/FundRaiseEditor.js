Ext.define('B4.controller.FundRaiseEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'FundRaiseEditorModel'],
    views: [
        'FundRaiseEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.FundRaiseEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-fundraiseeditor',
    viewDataModel: 'FundRaiseEditorModel',
    viewDataController: 'FundRaiseEditor',
    viewDataName: 'FundRaiseEditor',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.FundRaiseCreateEditor', 'B4.controller.WalletList']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Сбор.Создание
        element = form.down('[rmsUid=d5d74612-b8c2-47b4-acf7-afd6aaffb458]');
        ctrl = me.getController('B4.controller.FundRaiseCreateEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(rec.get('FundRaiseCreateEditor'));
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(model, element);
        // Кошельки
        element = form.down('[name=WalletList]');
        ctrl = me.getController('B4.controller.WalletList');
        element.data.set('FundRaise_Id', rec.get('Id') || 0);
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        // Сбор.Создание
        element = form.down('[rmsUid=d5d74612-b8c2-47b4-acf7-afd6aaffb458]');
        ctrl = me.getController('B4.controller.FundRaiseCreateEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(record.get('FundRaiseCreateEditor'));
        ctrl.onSetViewData(model, element);
        if (isNewRecord == true) {
            form.grid_WalletList.disableGrid();
        } else {
            form.grid_WalletList.enable();
            form.grid_WalletList.getStore().load();
        }
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Сбор.Создание
        element = view.down('[rmsUid=d5d74612-b8c2-47b4-acf7-afd6aaffb458]');
        ctrl = me.getController('B4.controller.FundRaiseCreateEditor');
        ctrl.connectView(element, view.ctxKey);
        // Кошельки
        element = view.down('[name=WalletList]');
        ctrl = me.getController('B4.controller.WalletList');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        // Сбор.Создание
        element = view.down('[rmsUid=d5d74612-b8c2-47b4-acf7-afd6aaffb458]');
        ctrl = me.getController('B4.controller.FundRaiseCreateEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(element.getEditorValues());
        ctrl.onViewDeployed(element, model);
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});