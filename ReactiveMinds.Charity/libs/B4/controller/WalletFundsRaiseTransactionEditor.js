Ext.define('B4.controller.WalletFundsRaiseTransactionEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'WalletFundsRaiseTransactionEditorModel'],
    views: [
        'WalletFundsRaiseTransactionEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.WalletFundsRaiseTransactionEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-walletfundsraisetransactioneditor',
    viewDataModel: 'WalletFundsRaiseTransactionEditorModel',
    viewDataController: 'WalletFundsRaiseTransactionEditor',
    viewDataName: 'WalletFundsRaiseTransactionEditor',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-walletfundsraisetransactioneditor': {
                'onViewDataChanged': function(view) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=23d66549-9764-4746-8c8e-22357c665b4b]'),
                        cmp1 = this.down('[rmsUid=2ed3e30c-7a37-443f-9f98-512903d3e33f]'),
                        cmpValue = cmp1.getValue(),
                        value = (cmp1.xtype == 'b4pickerfield' && cmp1.isGetOnlyIdProperty) ? (cmpValue && cmpValue != 0 ? {
                            Id: cmpValue
                        } : null) : cmpValue,
                        cmp2 = this.down('[rmsUid=47f78267-53bf-4631-bf62-fef7f1e52155]');
                    cmp2.setValue(['Поступление на кошелек "', cmp.getRawValue(), '" ', value, 'р.'].join(''));
                }
            }
        });
        me.control({
            scope: me,
            '[rmsUid=23d66549-9764-4746-8c8e-22357c665b4b]': {
                'change': function(sender, newValue, oldValue) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=23d66549-9764-4746-8c8e-22357c665b4b]'),
                        value = cmp && cmp.value && cmp.value['Currency_Id'],
                        filter = {
                            "Group": 2,
                            "Filters": [{
                                "DataIndex": "Id",
                                "Operand": 0,
                                "Value": value
                            }]
                        },
                        cmp1 = this.down('[rmsUid=4efcd5b4-9e23-4b79-bbde-c9626c67666c]');
                    if (!Ext.isEmpty(value)) {
                        cmp1.setValue(B4.ClientCode.recordByFilter('CurrencyList', filter));
                        B4.utils.FormHelper.makeInactive(cmp1, true);
                    } else {
                        B4.utils.FormHelper.makeInactive(cmp1, false);
                    }
                }
            }
        });
        me.callParent(arguments);
        this.initControllers(['B4.controller.FundRaiseTransactionEditor']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Транзакция сбора
        element = form.down('[rmsUid=a3d44b68-9c73-4269-836d-64e295c34fb2]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(rec.get('FundRaiseTransactionEditor'));
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(model, element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        // Транзакция сбора
        element = form.down('[rmsUid=a3d44b68-9c73-4269-836d-64e295c34fb2]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(record.get('FundRaiseTransactionEditor'));
        ctrl.onSetViewData(model, element);
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Транзакция сбора
        element = view.down('[rmsUid=a3d44b68-9c73-4269-836d-64e295c34fb2]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        // Транзакция сбора
        element = view.down('[rmsUid=a3d44b68-9c73-4269-836d-64e295c34fb2]');
        ctrl = me.getController('B4.controller.FundRaiseTransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(element.getEditorValues());
        ctrl.onViewDeployed(element, model);
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});