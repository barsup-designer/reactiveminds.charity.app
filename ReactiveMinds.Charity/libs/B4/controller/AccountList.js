Ext.define('B4.controller.AccountList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.AccountList',
        'B4.model.AccountListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-accountlist',
    viewDataName: 'AccountList',
    // набор описаний действий реестра
    actions: {
        'Addition-BankEditor-InWindow': {
            'editor': 'BankEditor',
            'editorAlias': 'rms-bankeditor',
            'editorUid': 'b0230488-b8ee-4af3-8dfb-1d2e0e20c487',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Addition-ClinicEditor-InWindow': {
            'editor': 'ClinicEditor',
            'editorAlias': 'rms-cliniceditor',
            'editorUid': '0988d306-df07-466b-b32f-06ff85a432bf',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Addition-PaymentSystemEditor-InWindow': {
            'editor': 'PaymentSystemEditor',
            'editorAlias': 'rms-paymentsystemeditor',
            'editorUid': 'e91a0429-c051-40d2-983c-25bbaa71ed91',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Addition-TelecomEditor-InWindow': {
            'editor': 'TelecomEditor',
            'editorAlias': 'rms-telecomeditor',
            'editorUid': '4ddd1e24-cad5-42ba-94a0-520e0d7c3f19',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow'
        },
        'Editing': {
            'editor': '',
            'editorAlias': '',
            'editorUid': '',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-accountlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-accountlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});