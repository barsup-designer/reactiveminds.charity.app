Ext.define('B4.controller.FundRaiseTransactionEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'FundRaiseTransactionEditorModel'],
    views: [
        'FundRaiseTransactionEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.FundRaiseTransactionEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-fundraisetransactioneditor',
    viewDataModel: 'FundRaiseTransactionEditorModel',
    viewDataController: 'FundRaiseTransactionEditor',
    viewDataName: 'FundRaiseTransactionEditor',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-fundraisetransactioneditor': {
                'onCreateRecord': function(cmp) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=d4e1a9b1-1a5b-41e2-bf56-2ff25eecf5d1]');
                    cmp.setValue(new Date());
                }
            }
        });
        me.callParent(arguments);
        this.initControllers(['B4.controller.TransactionEditor']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Транзакция
        element = form.down('[rmsUid=53be6419-df44-4c50-bd22-c0b5d908920d]');
        ctrl = me.getController('B4.controller.TransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(rec.get('TransactionEditor'));
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(model, element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['FundRaise_Id'])) {
            this.preventFieldInput(form, ['[name=FundRaise]']);
        }
        // Транзакция
        element = form.down('[rmsUid=53be6419-df44-4c50-bd22-c0b5d908920d]');
        ctrl = me.getController('B4.controller.TransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(record.get('TransactionEditor'));
        ctrl.onSetViewData(model, element);
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Транзакция
        element = view.down('[rmsUid=53be6419-df44-4c50-bd22-c0b5d908920d]');
        ctrl = me.getController('B4.controller.TransactionEditor');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        // Транзакция
        element = view.down('[rmsUid=53be6419-df44-4c50-bd22-c0b5d908920d]');
        ctrl = me.getController('B4.controller.TransactionEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(element.getEditorValues());
        ctrl.onViewDeployed(element, model);
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});