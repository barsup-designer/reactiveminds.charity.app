Ext.define('B4.controller.TelecomEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'TelecomEditorModel'],
    views: [
        'TelecomEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.TelecomEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-telecomeditor',
    viewDataModel: 'TelecomEditorModel',
    viewDataController: 'TelecomEditor',
    viewDataName: 'TelecomEditor',
    init: function() {
        var me = this;
        me.callParent(arguments);
        this.initControllers(['B4.controller.AccountEditor']);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
        // Счет
        element = form.down('[rmsUid=7c308911-7fef-4d04-8c19-f4b7ed831ce1]');
        ctrl = me.getController('B4.controller.AccountEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(rec.get('AccountEditor'));
        element.context && element.context.applyIf(_editorCtxValues);
        ctrl.applyCtxValues(model, element);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        // Счет
        element = form.down('[rmsUid=7c308911-7fef-4d04-8c19-f4b7ed831ce1]');
        ctrl = me.getController('B4.controller.AccountEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(record.get('AccountEditor'));
        ctrl.onSetViewData(model, element);
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
        // Счет
        element = view.down('[rmsUid=7c308911-7fef-4d04-8c19-f4b7ed831ce1]');
        ctrl = me.getController('B4.controller.AccountEditor');
        ctrl.connectView(element, view.ctxKey);
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        // Счет
        element = view.down('[rmsUid=7c308911-7fef-4d04-8c19-f4b7ed831ce1]');
        ctrl = me.getController('B4.controller.AccountEditor');
        model = ctrl.getModel(ctrl.viewDataModel).create(element.getEditorValues());
        ctrl.onViewDeployed(element, model);
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});