Ext.define('B4.controller.TransactionEditor', {
    extend: 'B4.base.form.Controller',
    models: [
        'TransactionEditorModel'],
    views: [
        'TransactionEditor'],
    requires: [
        'B4.aspects.Permission',
        'B4.view.TransactionEditor'],
    aspects: [{
        'permissions': [],
        'xtype': 'permissionaspect'
    }],
    // алиас контролируемых представлений
    viewAlias: 'rms-transactioneditor',
    viewDataModel: 'TransactionEditorModel',
    viewDataController: 'TransactionEditor',
    viewDataName: 'TransactionEditor',
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-transactioneditor': {
                'onViewDeployed': function(cmp, record) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=2ed3e30c-7a37-443f-9f98-512903d3e33f]');
                    B4.utils.FormHelper.makeInactive(cmp, true);
                }
            }
        });
        me.control({
            scope: me,
            'rms-transactioneditor': {
                'onViewDataChanged': function(view) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=d5aee8db-3ac9-45fa-85fc-969c75a07878]'),
                        cmpValue = cmp.getValue(),
                        value = (cmp.xtype == 'b4pickerfield' && cmp.isGetOnlyIdProperty) ? (cmpValue && cmpValue != 0 ? {
                            Id: cmpValue
                        } : null) : cmpValue,
                        cmp1 = this.down('[rmsUid=4efcd5b4-9e23-4b79-bbde-c9626c67666c]'),
                        value1 = cmp1 && cmp1.value && cmp1.value['ExchangeRate'],
                        cmp2 = this.down('[rmsUid=2ed3e30c-7a37-443f-9f98-512903d3e33f]');
                    cmp2.setValue(value * value1);
                }
            }
        });
        me.control({
            scope: me,
            'rms-transactioneditor': {
                'onCreateRecord': function(cmp) {
                    var _self = this,
                        _args = Array.prototype.slice.call(arguments);
                    var cmp = this.down('[rmsUid=4efcd5b4-9e23-4b79-bbde-c9626c67666c]');
                    cmp.setValue(B4.ClientCode.recordByField('CurrencyList', 'Id', '0', 1));
                }
            }
        });
        me.callParent(arguments);
    },
    applyCtxValues: function(rec, form) {
        var me = this,
            ctrl = null,
            element = null,
            model = null;
        var _ctrl = null,
            _ctxParams = null,
            _editorCtxValues = form.getEditorValues(true);
    },
    onSetViewData: function(record, form) {
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // получаем параметры контекста
        var ctxParams = form.data.getValues();
        // если в текущем контексте контроллера переданы идентификаторы
        // ссылочных полей, то необходимо передать их в параметрах операции 
        // и скрыть столбцы, отображающие поля ссылок
        if (!Ext.isEmpty(ctxParams['Currency_Id'])) {
            this.preventFieldInput(form, ['[name=Currency]']);
        }
        if (isNewRecord == true) {} else {}
    },
    onViewConnected: function(view) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null;
        // при привязке представления к контроллеру  необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и привязать их к их контроллерам
    },
    onViewDeployed: function(view, record) {
        this.callParent(arguments);
        var me = this,
            element = null,
            ctrl = null,
            model = null,
            isNewRecord = Ext.isEmpty(record.get('Id')) || record.get('Id') == 0;
        // после размещения представления необходимо обойти 
        // список вложенных представлений (реестров, редакторов и т.д.)
        // и выполнить допонительные методы инициализации
        // в данный момент параметры контекста заданы, 
        // компоненты созданы и отрендерены	
        if (isNewRecord == true) {} else {}
        if (view.isReadOnly) {
            view.setReadOnly();
        }
    },
});