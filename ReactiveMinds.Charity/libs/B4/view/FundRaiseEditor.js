Ext.define('B4.view.FundRaiseEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-fundraiseeditor',
    title: 'Сбор.Изменение',
    rmsUid: '008b0e16-cfe7-4308-97be-583ff8902252',
    requires: [
        'B4.form.SelectField',
        'B4.view.FundRaiseCreateEditor',
        'B4.view.WalletList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'FundRaise_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'FundRaiseCreateEditor',
        'modelProperty': 'FundRaiseCreateEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '008b0e16-cfe7-4308-97be-583ff8902252-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FundRaiseEditor-container',
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': null,
            'fieldLabel': 'Сбор.Создание',
            'height': 275,
            'idProperty': 'Id',
            'modelProperty': 'FundRaiseCreateEditor',
            'name': 'FundRaiseCreateEditor',
            'rmsUid': 'd5d74612-b8c2-47b4-acf7-afd6aaffb458',
            'tbar': null,
            'title': null,
            'xtype': 'rms-fundraisecreateeditor'
        }, {
            '$EntityFilter': {
                'LinkFilter': {
                    "Group": 3,
                    "Operand": 0,
                    "DataIndex": null,
                    "DataIndexType": null,
                    "Value": null,
                    "Filters": [{
                        "Group": 0,
                        "Operand": 0,
                        "DataIndex": "FundRaise.Id",
                        "DataIndexType": null,
                        "Value": "@FundRaise_Id",
                        "Filters": null
                    }]
                }
            },
            'closable': false,
            'fieldLabel': 'Кошельки',
            'flex': 1,
            'margin': '5 5 5 5',
            'name': 'WalletList',
            'rmsUid': '80cd81a6-d609-41bd-81f7-56aa202d6258',
            'title': null,
            'xtype': 'rms-walletlist'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'FundRaise_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_WalletList = me.down('rms-walletlist[name=WalletList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            var _titleValue = Ext.String.format('Сбор {0}', rec.get('Name'));
            me.setTitle(_titleValue);
        } else {}
        return res;
    },
});