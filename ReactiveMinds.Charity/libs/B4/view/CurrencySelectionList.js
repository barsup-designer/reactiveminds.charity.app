Ext.define('B4.view.CurrencySelectionList', {
    'alias': 'widget.rms-currencyselectionlist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.CurrencySelectionListModel',
    'stateful': true,
    'title': 'Реестр валют.Выбор',
    requires: [
        'B4.model.CurrencySelectionListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar',
        'B4.ux.NsiListView',
        'B4.ux.nsi.plugin.NsiGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '6171b69e-edd5-4e54-8872-1480c037ad99',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-CurrencyEditor-InWindow',
        'rmsUid': '59284ca5-8fdf-478a-9c35-7dc8c8dc6193',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-CurrencyEditor-InWindow',
        'rmsUid': '3f1194fb-0a11-4a6d-9d17-3804307d7077',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '7a65cd75-6dc8-4d64-a2a9-193eeeffc288',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'DisplayName',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '50268d78-9572-458e-8fb3-9c1ab9c444f3',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ExchangeRate',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'numberfield',
                    'allowDecimals': true
                },
                'flex': 1,
                'format': '0.000',
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '2f3f33b8-599d-4491-a842-c738c73d90cc',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Курс',
                'xtype': 'numbercolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'ptype': 'rms-nsi-listview'
            }, {
                'ptype': 'nsiGridPlugin'
            }]
        });
        me.callParent(arguments);
    }
});