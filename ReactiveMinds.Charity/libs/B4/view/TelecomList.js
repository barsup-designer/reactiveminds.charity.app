Ext.define('B4.view.TelecomList', {
    'alias': 'widget.rms-telecomlist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.TelecomListModel',
    'stateful': true,
    'title': 'Операторы связи',
    requires: [
        'B4.model.TelecomListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'cf5b7f93-1a8a-4cc9-b9c5-550f26801bcf',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-TelecomEditor-InWindow',
        'rmsUid': 'f270d0c3-bfba-48e5-a16f-8d23b3fcb077',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-TelecomEditor-InWindow',
        'rmsUid': '16d56b2d-47dd-4ce4-9344-54044f1246a2',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '778a85e5-0ebc-4142-b5a7-7d48b48e7d5a',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Name',
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'fa76f991-2487-4bcd-b28f-e85043eda626',
                'sortable': true,
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});