Ext.define('B4.view.WalletFundsRaiseTransactionEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-walletfundsraisetransactioneditor',
    title: 'Поступление средств на кошелек',
    rmsUid: '8bb0ed10-de66-4089-a912-5929f517c332',
    requires: [
        'B4.form.PickerField',
        'B4.form.SelectField',
        'B4.model.AccountEditorModel',
        'B4.model.WalletSelectionListModel',
        'B4.view.FundRaiseTransactionEditor',
        'B4.view.WalletSelectionList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'WalletFundsRaiseTransaction_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Target',
        'modelProperty': 'WalletFundsRaiseTransaction_Target',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'FundRaiseTransactionEditor',
        'modelProperty': 'FundRaiseTransactionEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '8bb0ed10-de66-4089-a912-5929f517c332-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'WalletFundsRaiseTransactionEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    '$SelectorFilter': {
                        "Group": 2,
                        "Operand": 0,
                        "DataIndex": null,
                        "DataIndexType": null,
                        "Value": null,
                        "Filters": [{
                            "Group": 0,
                            "Operand": 0,
                            "DataIndex": "FundRaise.Id",
                            "DataIndexType": null,
                            "Value": "@4b0c45b7-2d4e-4831-960a-04e25542a6bc_Id",
                            "Filters": null
                        }]
                    },
                    'allowBlank': false,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Кошелек',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.WalletSelectionList',
                    'listViewCtl': 'B4.controller.WalletSelectionList',
                    'maximizable': true,
                    'model': 'B4.model.WalletSelectionListModel',
                    'modelProperty': 'WalletFundsRaiseTransaction_Target',
                    'name': 'Target',
                    'rmsUid': '23d66549-9764-4746-8c8e-22357c665b4b',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Кошелек',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '2843eef4-dd38-40bc-99e7-c940c13c6048',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '1bf9f56c-8ac3-4041-b63d-79ef3050a443',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': null,
            'fieldLabel': 'Транзакция сбора',
            'idProperty': 'Id',
            'margin': '5 5 5 5',
            'modelProperty': 'FundRaiseTransactionEditor',
            'name': 'FundRaiseTransactionEditor',
            'rmsUid': 'a3d44b68-9c73-4269-836d-64e295c34fb2',
            'tbar': null,
            'title': null,
            'xtype': 'rms-fundraisetransactioneditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'WalletFundsRaiseTransaction_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});