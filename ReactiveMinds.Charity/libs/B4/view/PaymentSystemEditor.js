Ext.define('B4.view.PaymentSystemEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-paymentsystemeditor',
    title: 'Платежная система',
    rmsUid: 'e91a0429-c051-40d2-983c-25bbaa71ed91',
    requires: [
        'B4.form.SelectField',
        'B4.view.AccountEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'PaymentSystem_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'AccountEditor',
        'modelProperty': 'AccountEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'e91a0429-c051-40d2-983c-25bbaa71ed91-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PaymentSystemEditor-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            'dockedItems': null,
            'fieldLabel': 'Счет',
            'idProperty': 'Id',
            'modelProperty': 'AccountEditor',
            'name': 'AccountEditor',
            'rmsUid': '3d43abb4-9f21-4dbd-b973-f39e40304fa5',
            'tbar': null,
            'title': null,
            'xtype': 'rms-accounteditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'PaymentSystem_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});