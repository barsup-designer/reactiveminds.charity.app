Ext.define('B4.view.AccountEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-accounteditor',
    title: 'Счет',
    rmsUid: '338aa244-5c62-4602-a0af-339ebca1373d',
    requires: [
        'B4.form.PickerField',
        'B4.model.CurrencyEditorModel',
        'B4.model.CurrencyListModel',
        'B4.view.CurrencyList',
        'Ext.form.field.HtmlEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Account_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'Account_Name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Currency',
        'modelProperty': 'Account_Currency',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Description',
        'modelProperty': 'Account_Description',
        'type': 'HtmlEditorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '338aa244-5c62-4602-a0af-339ebca1373d-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'AccountEditor-container',
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Наименование',
                    'labelAlign': 'top',
                    'modelProperty': 'Account_Name',
                    'name': 'Name',
                    'rmsUid': '9479b9e3-f544-4cf0-8a89-b4685d8e239c',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'ecec232e-1a86-4aa3-8c77-0dedab4eae8f',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Валюта',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.CurrencyList',
                    'listViewCtl': 'B4.controller.CurrencyList',
                    'maximizable': true,
                    'model': 'B4.model.CurrencyListModel',
                    'modelProperty': 'Account_Currency',
                    'name': 'Currency',
                    'rmsUid': '277dd71d-810e-4eeb-9fa3-e6c38b8f9f1a',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Валюта',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '531995bb-7a71-4d7b-9865-63a8a985fcfc',
                'title': 'Вертикальный контейнер',
                'width': 150,
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '5ee114d4-f109-464e-b53e-13a3ad888b2f',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Описание',
            'flex': 1,
            'labelAlign': 'top',
            'margin': '5 5 5 5',
            'maxLength': 250,
            'maxLengthText': 'Превышена максимальная длина',
            'minLength': 0,
            'minLengthText': 'Текст короче минимальной длины',
            'modelProperty': 'Account_Description',
            'name': 'Description',
            'rmsUid': 'd1a49e36-80f6-4f14-bf5d-99925243a414',
            'xtype': 'htmleditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Account_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});