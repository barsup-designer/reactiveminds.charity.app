Ext.define('B4.view.WalletExchangeTransactionEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-walletexchangetransactioneditor',
    title: 'Перевод между кошельками',
    rmsUid: 'c64e0ad7-a884-4ce3-bf6f-fb4abd8b40a1',
    requires: [
        'B4.form.PickerField',
        'B4.form.SelectField',
        'B4.model.AccountEditorModel',
        'B4.model.WalletSelectionListModel',
        'B4.view.FundRaiseTransactionEditor',
        'B4.view.WalletSelectionList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'WalletExchangeTransaction_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Source',
        'modelProperty': 'WalletExchangeTransaction_Source',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Target',
        'modelProperty': 'WalletExchangeTransaction_Target',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'FundRaiseTransactionEditor',
        'modelProperty': 'FundRaiseTransactionEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'c64e0ad7-a884-4ce3-bf6f-fb4abd8b40a1-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'WalletExchangeTransactionEditor-container',
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    '$SelectorFilter': {
                        "Group": 2,
                        "Operand": 0,
                        "DataIndex": null,
                        "DataIndexType": null,
                        "Value": null,
                        "Filters": [{
                            "Group": 0,
                            "Operand": 0,
                            "DataIndex": "FundRaise.Id",
                            "DataIndexType": null,
                            "Value": "@4b0c45b7-2d4e-4831-960a-04e25542a6bc_Id",
                            "Filters": null
                        }]
                    },
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Источник',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.WalletSelectionList',
                    'listViewCtl': 'B4.controller.WalletSelectionList',
                    'maximizable': true,
                    'model': 'B4.model.WalletSelectionListModel',
                    'modelProperty': 'WalletExchangeTransaction_Source',
                    'name': 'Source',
                    'rmsUid': '735f74cd-3c46-42fc-be06-2a9aa59560be',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Источник',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '55ab9d16-b5ac-4b73-abe3-8cc55a5511e4',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    '$SelectorFilter': {
                        "Group": 2,
                        "Operand": 0,
                        "DataIndex": null,
                        "DataIndexType": null,
                        "Value": null,
                        "Filters": [{
                            "Group": 0,
                            "Operand": 0,
                            "DataIndex": "FundRaise.Id",
                            "DataIndexType": null,
                            "Value": "@4b0c45b7-2d4e-4831-960a-04e25542a6bc_Id",
                            "Filters": null
                        }]
                    },
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Приемник',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.WalletSelectionList',
                    'listViewCtl': 'B4.controller.WalletSelectionList',
                    'maximizable': true,
                    'model': 'B4.model.WalletSelectionListModel',
                    'modelProperty': 'WalletExchangeTransaction_Target',
                    'name': 'Target',
                    'rmsUid': 'd922b559-145d-4e2b-84b7-5fdc2de1ee73',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Приемник',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '6b12bf18-2ccc-4455-940b-5c98df63d99e',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '37df83c2-95b6-4438-a080-9a0394b85972',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'dockedItems': null,
            'fieldLabel': 'Транзакция сбора',
            'flex': 1,
            'idProperty': 'Id',
            'modelProperty': 'FundRaiseTransactionEditor',
            'name': 'FundRaiseTransactionEditor',
            'rmsUid': '57f27279-f224-44da-a716-010e647cb617',
            'tbar': null,
            'title': null,
            'xtype': 'rms-fundraisetransactioneditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'WalletExchangeTransaction_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});