Ext.define('B4.view.AccountList', {
    'alias': 'widget.rms-accountlist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.AccountListModel',
    'stateful': true,
    'title': 'Счета',
    requires: [
        'B4.model.AccountListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar',
        'B4.ux.grid.plugin.RowExpander'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'f8bc0e55-146e-4d41-a009-a64323345a76',
        'text': 'Обновить'
    }, {
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Menu',
        'menu': [{
            'actionName': 'add',
            'hidden': false,
            'iconCls': 'content-img-icons-money-png',
            'itemId': 'Addition-BankEditor-InWindow',
            'rmsUid': 'fa2af25b-b833-402d-a5e7-e850c8d65ef4',
            'text': 'Банк'
        }, {
            'actionName': 'add',
            'hidden': false,
            'iconCls': 'content-img-icons-phone-png',
            'itemId': 'Addition-TelecomEditor-InWindow',
            'rmsUid': 'af48800a-ec9c-4501-9adf-cc4d171b51cd',
            'text': 'Оператор связи'
        }, {
            'actionName': 'add',
            'hidden': false,
            'iconCls': 'content-img-icons-money_euro-png',
            'itemId': 'Addition-PaymentSystemEditor-InWindow',
            'rmsUid': '74c09954-6bc9-400d-ae55-ff03d21ba7da',
            'text': 'Платежная система'
        }, {
            'actionName': 'add',
            'hidden': false,
            'iconCls': 'content-img-icons-asterisk_red-png',
            'itemId': 'Addition-ClinicEditor-InWindow',
            'rmsUid': '5301c56b-120e-4cee-845c-f8dd44b0546b',
            'text': 'Добавить'
        }],
        'rmsUid': '307314f4-5a8a-46f2-b5bb-2ba1e6e866cc',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-img-icons-pencil-png',
        'itemId': 'Editing',
        'rmsUid': '1356600b-774f-4e48-8c01-e60a4862029b',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '28a6632d-03ea-4e8e-aff7-4bedc6587dff',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Name',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '4d040305-225e-481b-a536-9fd38fd305b6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'b4rowexpander',
                'rowBodyTpl': new Ext.XTemplate('<p><b>Описание: </b> {Description}</p> <br />\
')
            }]
        });
        me.callParent(arguments);
    }
});