Ext.define('B4.view.BankList', {
    'alias': 'widget.rms-banklist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.BankListModel',
    'stateful': true,
    'title': 'Банки',
    requires: [
        'B4.model.BankListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '36d2ccae-8d90-46aa-b229-750e67841b9c',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-BankEditor-InWindow',
        'rmsUid': '605a08e7-f3d5-410c-8e20-87612a6c512e',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-BankEditor-InWindow',
        'rmsUid': 'c0b2209d-9bf8-467e-bb88-8d561f5eebe4',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '35b67830-4fdf-44f5-9330-06e04ba9b3dd',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Name',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '98ca39b3-9f65-46f7-817f-bbd6186d05b4',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});