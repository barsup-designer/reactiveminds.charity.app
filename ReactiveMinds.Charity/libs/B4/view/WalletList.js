Ext.define('B4.view.WalletList', {
    'alias': 'widget.rms-walletlist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.WalletListModel',
    'stateful': true,
    'title': 'Кошельки',
    requires: [
        'B4.model.WalletListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar',
        'B4.ux.grid.plugin.RowExpander'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'fc8bb431-fca1-4f94-87f1-3b237af58c53',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-WalletEditor-InWindow',
        'rmsUid': 'de46c072-530e-4917-b539-482b2db671a0',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-WalletEditor-InWindow',
        'rmsUid': '38b339f5-cfe0-4589-ac91-71e9d735608c',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '6b2a65cb-184d-4eae-ba23-f93b1fbd8d16',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Balance',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'numberfield',
                    'allowDecimals': true
                },
                'format': '0.000',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3e3b3b84-3495-48b2-a443-6b577a8e48f6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Баланс',
                'width': 120,
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'Name',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '81699f46-0794-4c7d-9129-329221fc2199',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FundRaise_Id',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'd5a747e6-0bf9-491d-bad2-56208aa31c6c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Сбор.Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Currency_Id',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ea53f2f9-82f3-4b95-ac3f-5c851b149c39',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Валюта счета.Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'b4rowexpander',
                'rowBodyTpl': new Ext.XTemplate('<p><b>Описание: </b> {Description}</p> <br />\
')
            }]
        });
        me.callParent(arguments);
    }
});