Ext.define('B4.view.TelecomEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-telecomeditor',
    title: 'Оператор связи',
    rmsUid: '4ddd1e24-cad5-42ba-94a0-520e0d7c3f19',
    requires: [
        'B4.form.SelectField',
        'B4.view.AccountEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Telecom_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'AccountEditor',
        'modelProperty': 'AccountEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '4ddd1e24-cad5-42ba-94a0-520e0d7c3f19-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TelecomEditor-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            'dockedItems': null,
            'fieldLabel': 'Счет',
            'idProperty': 'Id',
            'modelProperty': 'AccountEditor',
            'name': 'AccountEditor',
            'rmsUid': '7c308911-7fef-4d04-8c19-f4b7ed831ce1',
            'tbar': null,
            'title': null,
            'xtype': 'rms-accounteditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Telecom_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});