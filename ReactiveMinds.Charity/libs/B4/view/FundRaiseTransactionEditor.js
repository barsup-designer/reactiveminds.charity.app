Ext.define('B4.view.FundRaiseTransactionEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-fundraisetransactioneditor',
    title: 'Транзакция сбора',
    rmsUid: '43aaae1b-fe2f-407c-a2d1-cc7e38f153bf',
    requires: [
        'B4.form.PickerField',
        'B4.form.SelectField',
        'B4.model.FundRaiseListModel',
        'B4.model.FundRaiseTreeEditorModel',
        'B4.view.FundRaiseList',
        'B4.view.TransactionEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'FundRaiseTransaction_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'FundRaise',
        'modelProperty': 'FundRaiseTransaction_FundRaise',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'TransactionDate',
        'modelProperty': 'FundRaiseTransaction_TransactionDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'TransactionEditor',
        'modelProperty': 'TransactionEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '43aaae1b-fe2f-407c-a2d1-cc7e38f153bf-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FundRaiseTransactionEditor-container',
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Сбор',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.FundRaiseList',
                    'listViewCtl': 'B4.controller.FundRaiseList',
                    'model': 'B4.model.FundRaiseListModel',
                    'modelProperty': 'FundRaiseTransaction_FundRaise',
                    'name': 'FundRaise',
                    'rmsUid': '4b0c45b7-2d4e-4831-960a-04e25542a6bc',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Сбор',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '809337a4-8a04-42c8-9237-b7e9ee01d128',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Дата',
                    'format': 'd.m.Y',
                    'labelAlign': 'top',
                    'modelProperty': 'FundRaiseTransaction_TransactionDate',
                    'name': 'TransactionDate',
                    'rmsUid': 'd4e1a9b1-1a5b-41e2-bf56-2ff25eecf5d1',
                    'xtype': 'datefield'
                }],
                'layout': 'anchor',
                'rmsUid': 'aeae9db2-d88d-4601-8bff-1de4f7268fcb',
                'title': 'Вертикальный контейнер',
                'width': 150,
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '8f09846f-3481-4593-a8c9-5a8b58bd3053',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'dockedItems': null,
            'fieldLabel': 'Транзакция',
            'flex': 1,
            'idProperty': 'Id',
            'modelProperty': 'TransactionEditor',
            'name': 'TransactionEditor',
            'rmsUid': '53be6419-df44-4c50-bd22-c0b5d908920d',
            'tbar': null,
            'title': null,
            'xtype': 'rms-transactioneditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'FundRaiseTransaction_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});