Ext.define('B4.view.CurrencyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-currencyeditor',
    title: 'Валюта',
    rmsUid: 'dfc6489a-ff0f-487d-8839-1a9856aef807',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Currency_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Code',
        'modelProperty': 'Currency_Code',
        'type': 'TextField'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'Currency_Name',
        'type': 'TextField'
    }, {
        'dataIndex': 'ExchangeRate',
        'modelProperty': 'Currency_ExchangeRate',
        'type': 'NumberField'
    }, {
        'dataIndex': 'StartDate',
        'modelProperty': 'Currency_StartDate',
        'type': 'DateField'
    }, {
        'dataIndex': 'EndDate',
        'modelProperty': 'Currency_EndDate',
        'type': 'DateField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'dfc6489a-ff0f-487d-8839-1a9856aef807-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'CurrencyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'fieldLabel': 'Код',
                    'labelAlign': 'top',
                    'modelProperty': 'Currency_Code',
                    'name': 'Code',
                    'rmsUid': 'fac4e3d5-8a0f-4df5-b665-597f47cfc391',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'f74c6748-eb50-41de-808e-cd7b9a389bdb',
                'title': 'Вертикальный контейнер',
                'width': 120,
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'fieldLabel': 'Наименование',
                    'labelAlign': 'top',
                    'modelProperty': 'Currency_Name',
                    'name': 'Name',
                    'rmsUid': '45ebed11-5e23-49fc-9df1-1b5029745119',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'cc77ce0e-b379-4167-be1f-998b40c66c0f',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Курс',
                    'labelAlign': 'top',
                    'modelProperty': 'Currency_ExchangeRate',
                    'name': 'ExchangeRate',
                    'rmsUid': '05189555-a2f1-4433-b20a-165f42067620',
                    'xtype': 'numberfield'
                }],
                'layout': 'anchor',
                'rmsUid': '309587a8-3626-4d1f-9f72-00df4ff51b62',
                'title': 'Вертикальный контейнер',
                'width': 120,
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '426ade56-c3b7-4e03-8dbd-f0828d93a514',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'fieldLabel': 'Дата начала',
                    'format': 'd.m.Y',
                    'labelAlign': 'top',
                    'modelProperty': 'Currency_StartDate',
                    'name': 'StartDate',
                    'rmsUid': 'fd45e2ad-fcfc-4f8d-9aab-40b7fce59ab6',
                    'xtype': 'datefield'
                }],
                'layout': 'anchor',
                'rmsUid': 'ded12abc-2828-48ab-b476-e69cbd553914',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'fieldLabel': 'Дата окончания',
                    'format': 'd.m.Y',
                    'labelAlign': 'top',
                    'modelProperty': 'Currency_EndDate',
                    'name': 'EndDate',
                    'rmsUid': '127be122-d1ad-448d-82d0-fb6b21fd4e4f',
                    'xtype': 'datefield'
                }],
                'layout': 'anchor',
                'rmsUid': '7fe8ff81-8796-4697-b137-97e51edfc201',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '9f1a376f-1fc4-41a8-bbb8-4706704a0e4b',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Currency_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});