Ext.define('B4.view.ClinicEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-cliniceditor',
    title: 'Клиника',
    rmsUid: '0988d306-df07-466b-b32f-06ff85a432bf',
    requires: [
        'B4.form.SelectField',
        'B4.view.AccountEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Clinic_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'AccountEditor',
        'modelProperty': 'AccountEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '0988d306-df07-466b-b32f-06ff85a432bf-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'ClinicEditor-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            'dockedItems': null,
            'fieldLabel': 'Счет',
            'idProperty': 'Id',
            'modelProperty': 'AccountEditor',
            'name': 'AccountEditor',
            'rmsUid': 'a3dffbe5-1659-425e-9329-edd9b5f77c7a',
            'tbar': null,
            'title': null,
            'xtype': 'rms-accounteditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Clinic_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});