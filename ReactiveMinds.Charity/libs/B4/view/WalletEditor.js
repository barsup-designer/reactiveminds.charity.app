Ext.define('B4.view.WalletEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-walleteditor',
    title: 'Кошелек',
    rmsUid: '8c88219d-6274-4f41-bdc8-1610a2666faf',
    requires: [
        'B4.form.PickerField',
        'B4.form.SelectField',
        'B4.model.AccountEditorModel',
        'B4.model.AccountListModel',
        'B4.model.FundRaiseListModel',
        'B4.model.FundRaiseTreeEditorModel',
        'B4.view.AccountEditor',
        'B4.view.AccountList',
        'B4.view.FundRaiseList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Wallet_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'FundRaise',
        'modelProperty': 'Wallet_FundRaise',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Account',
        'modelProperty': 'Wallet_Account',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'AccountEditor',
        'modelProperty': 'AccountEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '8c88219d-6274-4f41-bdc8-1610a2666faf-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'WalletEditor-container',
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Сбор',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.FundRaiseList',
                    'listViewCtl': 'B4.controller.FundRaiseList',
                    'maximizable': true,
                    'model': 'B4.model.FundRaiseListModel',
                    'modelProperty': 'Wallet_FundRaise',
                    'name': 'FundRaise',
                    'rmsUid': '331228ac-e958-4134-bf04-c67a7b6830ba',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Сбор',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'f24d617f-4dbe-48fb-bf36-dbb15f742ba3',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Базируется на',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.AccountList',
                    'listViewCtl': 'B4.controller.AccountList',
                    'maximizable': true,
                    'model': 'B4.model.AccountListModel',
                    'modelProperty': 'Wallet_Account',
                    'name': 'Account',
                    'rmsUid': 'c02ff172-d2d4-40de-bb62-de813f43c9a4',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Базируется на',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': '2c686608-bb93-4794-87b2-e9b0fb11db98',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'c6ccc701-0d68-49bd-8c61-66fe733dee19',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'dockedItems': null,
            'fieldLabel': 'Счет',
            'flex': 1,
            'idProperty': 'Id',
            'modelProperty': 'AccountEditor',
            'name': 'AccountEditor',
            'rmsUid': '36491c45-16c9-4109-959b-524cf36ce43d',
            'tbar': null,
            'title': null,
            'xtype': 'rms-accounteditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Wallet_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            var _titleValue = Ext.String.format('Кошелек {0}', rec.get('Name'));
            me.setTitle(_titleValue);
        } else {}
        return res;
    },
});