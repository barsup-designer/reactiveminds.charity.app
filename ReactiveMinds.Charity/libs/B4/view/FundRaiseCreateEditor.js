Ext.define('B4.view.FundRaiseCreateEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-fundraisecreateeditor',
    title: 'Сбор.Создание',
    rmsUid: 'c5500f61-b5e5-4ee8-9ebf-86c0f331bccc',
    requires: [
        'Ext.form.field.HtmlEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'FundRaise_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'FundRaise_Name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Description',
        'modelProperty': 'FundRaise_Description',
        'type': 'HtmlEditorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'c5500f61-b5e5-4ee8-9ebf-86c0f331bccc-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FundRaiseCreateEditor-container',
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Наименование',
                    'labelAlign': 'top',
                    'modelProperty': 'FundRaise_Name',
                    'name': 'Name',
                    'rmsUid': '7fe2c995-ef6d-418f-ba23-8795d292d634',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'cccad4c0-6bb1-4e0f-be19-2a2a8205f3df',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'bf53c8cc-741f-41fd-b20c-23c4ded3d370',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Описание',
            'flex': 1,
            'labelAlign': 'top',
            'margin': '5 5 5 5',
            'modelProperty': 'FundRaise_Description',
            'name': 'Description',
            'rmsUid': '2f4c3b76-8d18-4114-801c-99b28351f332',
            'xtype': 'htmleditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'FundRaise_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {
            me.setTitle('Регистрация сбора');
        }
        return res;
    },
});