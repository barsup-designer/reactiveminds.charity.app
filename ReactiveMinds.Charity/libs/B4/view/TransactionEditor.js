Ext.define('B4.view.TransactionEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-transactioneditor',
    title: 'Транзакция',
    rmsUid: '50ec1384-cdbd-4e01-8400-792eaa33a261',
    requires: [
        'B4.form.PickerField',
        'B4.model.CurrencyEditorModel',
        'B4.model.CurrencySelectionListModel',
        'B4.overrides.form.field.Number',
        'B4.view.CurrencySelectionList',
        'Ext.form.field.HtmlEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Transaction_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'AmountInCurrency',
        'modelProperty': 'Transaction_AmountInCurrency',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Currency',
        'modelProperty': 'Transaction_Currency',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'AmountInRubles',
        'modelProperty': 'Transaction_AmountInRubles',
        'type': 'NumberField'
    }, {
        'dataIndex': 'Description',
        'modelProperty': 'Transaction_Description',
        'type': 'HtmlEditorField'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'Transaction_Name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '50ec1384-cdbd-4e01-8400-792eaa33a261-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'TransactionEditor-container',
        'layout': {
            'align': 'stretch',
            'type': 'vbox'
        },
        items: [{
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'decimalPrecision': 2,
                    'fieldLabel': 'Сумма в валюте',
                    'labelAlign': 'top',
                    'minValue': 0,
                    'modelProperty': 'Transaction_AmountInCurrency',
                    'name': 'AmountInCurrency',
                    'rmsUid': 'd5aee8db-3ac9-45fa-85fc-969c75a07878',
                    'step': 1,
                    'useGroupSeparator': true,
                    'xtype': 'numberfield'
                }],
                'layout': 'anchor',
                'rmsUid': '692d337d-b43d-4671-b1c6-e3e50270655e',
                'title': 'Вертикальный контейнер',
                'width': 120,
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Валюта',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.CurrencySelectionList',
                    'listViewCtl': 'B4.controller.CurrencySelectionList',
                    'maximizable': true,
                    'mode': 'grid',
                    'model': 'B4.model.CurrencySelectionListModel',
                    'modelProperty': 'Transaction_Currency',
                    'name': 'Currency',
                    'quickSearchEnabled': true,
                    'quickSearchMaxRecordsCount': 5,
                    'rmsUid': '4efcd5b4-9e23-4b79-bbde-c9626c67666c',
                    'textProperty': 'DisplayName',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Валюта',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'c685a493-e796-44ed-a3f6-a468805d0e20',
                'title': 'Вертикальный контейнер',
                'xtype': 'container'
            }, {
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'decimalPrecision': 2,
                    'fieldLabel': 'Сумма в рублях',
                    'labelAlign': 'top',
                    'modelProperty': 'Transaction_AmountInRubles',
                    'name': 'AmountInRubles',
                    'rmsUid': '2ed3e30c-7a37-443f-9f98-512903d3e33f',
                    'step': 1,
                    'useGroupSeparator': true,
                    'xtype': 'numberfield'
                }],
                'layout': 'anchor',
                'rmsUid': '6dcb3c1f-ba51-4921-b6e0-7321fbec5acb',
                'title': 'Вертикальный контейнер',
                'width': 120,
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'ace3bb98-620c-49b2-8eb9-bb41c8f89c3a',
            'title': 'Горизонтальный контейнер',
            'xtype': 'container'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Комментарий',
            'flex': 1,
            'labelAlign': 'top',
            'margin': '5 5 5 5',
            'modelProperty': 'Transaction_Description',
            'name': 'Description',
            'rmsUid': '0d297cc5-1946-4fe2-88cc-179008cc2849',
            'xtype': 'htmleditor'
        }, {
            'allowBlank': true,
            'fieldLabel': 'Наименование (авто)',
            'hidden': true,
            'maxLength': 500,
            'maxLengthText': 'Превышена максимальная длина',
            'minLength': 0,
            'minLengthText': 'Текст короче минимальной длины',
            'modelProperty': 'Transaction_Name',
            'name': 'Name',
            'rmsUid': '47f78267-53bf-4631-bf62-fef7f1e52155',
            'xtype': 'textfield'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Transaction_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});