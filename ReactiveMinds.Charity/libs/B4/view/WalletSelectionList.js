Ext.define('B4.view.WalletSelectionList', {
    'alias': 'widget.rms-walletselectionlist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.WalletSelectionListModel',
    'stateful': true,
    'title': 'Кошельки.Выбор',
    requires: [
        'B4.model.WalletSelectionListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar',
        'B4.ux.grid.plugin.RowExpander'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'd9e40b58-7c22-410f-a0d5-135537c112d7',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-WalletEditor-InWindow',
        'rmsUid': '1a8d39ff-c25f-4dfb-a188-f20d47919477',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-WalletEditor-InWindow',
        'rmsUid': '79ec1145-703c-4727-9b39-b8a414b06cc9',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '74b75198-413a-46c4-8987-ae9e15f37329',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Name',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '8afdeef7-3654-499a-99fd-ca9ceb5fe1d6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FundRaise_Id',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '909b0bd1-6869-47a6-b2ec-8b27f62dedd3',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Сбор.Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Currency_Id',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '16677c69-af50-40ff-b3e5-ff3e37a5d76c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Валюта счета.Идентификатор',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }, {
                'ptype': 'b4rowexpander',
                'rowBodyTpl': new Ext.XTemplate('<p><b>Описание: </b> {Description}</p> <br />\
')
            }]
        });
        me.callParent(arguments);
    }
});