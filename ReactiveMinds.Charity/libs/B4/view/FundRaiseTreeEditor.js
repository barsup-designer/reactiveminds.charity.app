Ext.define('B4.view.FundRaiseTreeEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-fundraisetreeeditor',
    title: 'Сбор.Дерево',
    rmsUid: 'fcfb5470-7851-45c4-ae61-3c33240abc41',
    requires: [
        'B4.view.NavigationPanel'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'FundRaise_Id',
        'type': 'hidden'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'fcfb5470-7851-45c4-ae61-3c33240abc41-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FundRaiseTreeEditor-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            'border': false,
            'fieldLabel': 'Панель навигации',
            'items': [],
            'rmsUid': '3d1a9409-e9f5-46bf-a49c-e22338051b00',
            'rootKey': '3d1a9409-e9f5-46bf-a49c-e22338051b00',
            'title': null,
            'treeWidth': 250,
            'xtype': 'rms-navigationpanel'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'FundRaise_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});