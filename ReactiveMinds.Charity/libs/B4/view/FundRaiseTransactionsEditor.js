Ext.define('B4.view.FundRaiseTransactionsEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-fundraisetransactionseditor',
    title: 'Сбор.Движение',
    rmsUid: 'ecdaa7cb-513a-4c47-b9dd-627f3254caaf',
    requires: [
        'B4.view.FundRaiseTransactionList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'FundRaise_Id',
        'type': 'hidden'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'ecdaa7cb-513a-4c47-b9dd-627f3254caaf-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'FundRaiseTransactionsEditor-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            '$EntityFilter': {
                'LinkFilter': {
                    "Group": 3,
                    "Operand": 0,
                    "DataIndex": null,
                    "DataIndexType": null,
                    "Value": null,
                    "Filters": [{
                        "Group": 0,
                        "Operand": 0,
                        "DataIndex": "FundRaise.Id",
                        "DataIndexType": null,
                        "Value": "@FundRaise_Id",
                        "Filters": null
                    }]
                }
            },
            'closable': false,
            'fieldLabel': 'Транзакции сбора',
            'name': 'FundRaiseTransactionList',
            'rmsUid': 'e6521d9e-31ae-4f27-a9d7-abe3b7c44fc8',
            'title': null,
            'xtype': 'rms-fundraisetransactionlist'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'FundRaise_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
        me.grid_FundRaiseTransactionList = me.down('rms-fundraisetransactionlist[name=FundRaiseTransactionList]');
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});