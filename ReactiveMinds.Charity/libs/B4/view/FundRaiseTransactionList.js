Ext.define('B4.view.FundRaiseTransactionList', {
    'alias': 'widget.rms-fundraisetransactionlist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.FundRaiseTransactionListModel',
    'stateful': true,
    'title': 'Транзакции сбора',
    requires: [
        'B4.model.FundRaiseTransactionListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '8f343222-c602-4a2e-bff6-08c35cea656e',
        'text': 'Обновить'
    }, {
        'hidden': false,
        'iconCls': 'content-filter-bar-images-add-png',
        'itemId': 'Menu',
        'menu': [{
            'actionName': 'add',
            'hidden': false,
            'iconCls': 'content-img-icons-money_add-png',
            'itemId': 'Addition-WalletFundsRaiseTransactionEditor-InWindow',
            'rmsUid': '98cb7ca3-1ccb-4036-b1df-b37917db46aa',
            'text': 'Поступление'
        }, {
            'actionName': 'add',
            'hidden': false,
            'iconCls': 'content-img-icons-arrow_switch_bluegreen-png',
            'itemId': 'Addition-WalletExchangeTransactionEditor-InWindow',
            'rmsUid': '27ea1fc7-80ff-45ae-97ef-d13dd3febc6a',
            'text': 'Перевод между кошельками'
        }],
        'rmsUid': '03f71916-9de1-4d14-befb-58ffe56f2c8b',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'content-img-icons-pencil-png',
        'itemId': 'Editing',
        'rmsUid': 'e161bb49-f54e-4b1e-9c8f-40bb35e71082',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '4250fc40-3a2a-4e9a-bbfe-71de1918c934',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'TransactionDate',
                'decimalPrecision': 2,
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': '718a0abe-9f4b-4711-bf87-c819f869aea3',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата',
                'width': 150,
                'xtype': 'gridcolumn'
            }, {
                'columns': [{
                    'dataIndex': 'Currency_Name',
                    'decimalPrecision': 2,
                    'filter': true,
                    'menuDisabled': false,
                    'multisortable': false,
                    'rmsUid': '81be50ef-1522-4fa8-bbd3-55ff54cd3142',
                    'sortable': true,
                    'summaryRenderer': function(value) {
                        return Ext.String.format('{0}', value);
                    },
                    'summaryType': 'none',
                    'text': 'Валюта',
                    'width': 120,
                    'xtype': 'gridcolumn'
                }, {
                    'dataIndex': 'AmountInCurrency',
                    'decimalPrecision': 2,
                    'filter': {
                        'xtype': 'numberfield',
                        'allowDecimals': true
                    },
                    'format': '0.000',
                    'menuDisabled': false,
                    'multisortable': false,
                    'rmsUid': '57d5ff34-f9af-404b-8b91-97f1256b0bea',
                    'sortable': true,
                    'summaryRenderer': function(value) {
                        return Ext.String.format('{0}', Ext.util.Format.number(value, '0,000.00'));
                    },
                    'summaryType': 'none',
                    'text': 'Сумма',
                    'width': 120,
                    'xtype': 'numbercolumn'
                }, {
                    'dataIndex': 'AmountInRubles',
                    'decimalPrecision': 2,
                    'filter': {
                        'xtype': 'numberfield',
                        'allowDecimals': true
                    },
                    'format': '0.000',
                    'menuDisabled': false,
                    'multisortable': false,
                    'rmsUid': '6fccc8c2-cb07-4a46-a4cf-112208cde47b',
                    'sortable': true,
                    'summaryRenderer': function(value) {
                        return Ext.String.format('{0}', Ext.util.Format.number(value, '0,000.00'));
                    },
                    'summaryType': 'none',
                    'text': 'В рублях',
                    'width': 120,
                    'xtype': 'numbercolumn'
                }],
                'flex': 1,
                'menuDisabled': true,
                'multisortable': false,
                'rmsUid': '8de2b557-e105-4b55-9bef-cf00b1731961',
                'sortable': false,
                'text': 'Сумма',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FundRaise_Id',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'hidden': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3b2edbe6-d393-43d3-95dd-7620ff4ab92e',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Сбор.Идентификатор',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Name',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '49c06aac-d8e4-46b7-8631-c192827236a4',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Комментрий',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Source_Name',
                'decimalPrecision': 2,
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '5e73e2fe-e335-49ad-aae6-9d46e14a089f',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Источник',
                'width': 120,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Target_Name',
                'decimalPrecision': 2,
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '99353922-ef1e-4aa1-8903-e792ad1c49a1',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Приемник',
                'width': 120,
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});