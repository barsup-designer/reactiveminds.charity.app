Ext.define('B4.view.BankEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-bankeditor',
    title: 'Банк',
    rmsUid: 'b0230488-b8ee-4af3-8dfb-1d2e0e20c487',
    requires: [
        'B4.form.SelectField',
        'B4.view.AccountEditor'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Bank_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'AccountEditor',
        'modelProperty': 'AccountEditor',
        'type': 'Form'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'text': 'Сохранить'
        }, {
            'actionName': 'saveclose',
            'iconCls': 'icon-disk-black',
            'text': 'Сохранить и закрыть'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'b0230488-b8ee-4af3-8dfb-1d2e0e20c487-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'BankEditor-container',
        'layout': {
            'type': 'fit'
        },
        items: [{
            'dockedItems': null,
            'fieldLabel': 'Счет',
            'idProperty': 'Id',
            'modelProperty': 'AccountEditor',
            'name': 'AccountEditor',
            'rmsUid': 'a55dcb0f-752a-4136-9d2c-e690926ddcd7',
            'tbar': null,
            'title': null,
            'xtype': 'rms-accounteditor'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Bank_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});