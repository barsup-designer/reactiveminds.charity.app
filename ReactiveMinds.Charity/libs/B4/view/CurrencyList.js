Ext.define('B4.view.CurrencyList', {
    'alias': 'widget.rms-currencylist',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.CurrencyListModel',
    'stateful': true,
    'title': 'Реестр валют',
    requires: [
        'B4.model.CurrencyListModel',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar',
        'B4.ux.NsiListView',
        'B4.ux.nsi.plugin.NsiGridPlugin'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'f3bc7212-6318-4602-9aff-325c41c319c2',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-CurrencyEditor-InWindow',
        'rmsUid': '539efb80-243d-4789-a729-1abdb1d5796c',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-CurrencyEditor-InWindow',
        'rmsUid': '2044f78d-1e7c-4149-b413-1897cd46bedf',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': '4a2bda54-25da-4217-ab9c-134f64db3f22',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Code',
                'decimalPrecision': 2,
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'ffe56b34-558b-427c-a33f-58140c18ea76',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Код',
                'width': 120,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'ExchangeRate',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'numberfield',
                    'allowDecimals': true
                },
                'format': '0.000',
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '1b130c30-1daa-4da7-8057-15d80e754871',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Курс',
                'width': 120,
                'xtype': 'numbercolumn'
            }, {
                'dataIndex': 'Name',
                'decimalPrecision': 2,
                'filter': true,
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '56ae526f-dffb-4bed-85c6-81ad1fce4927',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'StartDate',
                'decimalPrecision': 2,
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'e77fda1a-f9f2-48f8-8351-3af565e481a3',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата начала',
                'width': 150,
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'EndDate',
                'decimalPrecision': 2,
                'filter': true,
                'menuDisabled': false,
                'multisortable': false,
                'renderer': B4.grid.Renderers.date,
                'rmsUid': 'afaaa54d-32d4-4574-a285-2bd01a21e65c',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Дата окончания',
                'width': 150,
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'ptype': 'rms-nsi-listview'
            }, {
                'ptype': 'nsiGridPlugin'
            }]
        });
        me.callParent(arguments);
    }
});