namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4;
    using System.Linq;

    using Bars.B4;
    using Bars.B4.Utils;
    using Bars.B4.DataAccess;
    using Bars.B4.Application;

    using Castle.Windsor;
    using Bars.B4.Modules.NHibernateChangeLog;


    /// <summary>
    /// Провайдер логируемых сущностей
    /// </summary>
    public class AuditLogMapProvider : IAuditLogMapProvider
    {
        /// <summary>
        /// Инициализация
        /// </summary>
        /// <param name="container">Контейнер реализаций логируемых сущностей</param>
        public void Init(IAuditLogMapContainer container)
        {
        }
    }

}