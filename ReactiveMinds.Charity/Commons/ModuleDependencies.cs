namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4;
    using System.Linq;

    using Bars.B4;
    using Bars.B4.DataAccess;

    using Castle.Windsor;

    public class ModuleDependencies : BaseModuleDependencies
    {
        public ModuleDependencies(IWindsorContainer container)
            : base(container)
        {

            #region Счет
            References.Add(new EntityReference
            {
                ReferenceName = "Перевод между кошельками -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Any(x => x.Source.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Count(x => x.Source.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Перевод между кошельками -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Any(x => x.Target.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Count(x => x.Target.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Транзакция -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Transaction>().GetAll().Any(x => x.Source.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Transaction>().GetAll().Count(x => x.Source.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Транзакция -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Transaction>().GetAll().Any(x => x.Target.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Transaction>().GetAll().Count(x => x.Target.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Транзакция сбора -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Any(x => x.Source.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Count(x => x.Source.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Транзакция сбора -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Any(x => x.Target.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Count(x => x.Target.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Вывод средств -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.OutTransaction>().GetAll().Any(x => x.Source.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.OutTransaction>().GetAll().Count(x => x.Source.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Вывод средств -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.OutTransaction>().GetAll().Any(x => x.Target.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.OutTransaction>().GetAll().Count(x => x.Target.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Кошелек -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll().Any(x => x.Account.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll().Count(x => x.Account.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Поступление средств на кошелек -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Any(x => x.Source.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Count(x => x.Source.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Поступление средств на кошелек -> Счет",
                BaseEntity = typeof(ReactiveMinds.Charity.Account),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Any(x => x.Target.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Count(x => x.Target.Id == id),
                GetDescriptionDependences = id => ""
            });

            #endregion

            #region Сбор
            References.Add(new EntityReference
            {
                ReferenceName = "Перевод между кошельками -> Сбор",
                BaseEntity = typeof(ReactiveMinds.Charity.FundRaise),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Any(x => x.FundRaise.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Count(x => x.FundRaise.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Транзакция сбора -> Сбор",
                BaseEntity = typeof(ReactiveMinds.Charity.FundRaise),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Any(x => x.FundRaise.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Count(x => x.FundRaise.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Кошелек -> Сбор",
                BaseEntity = typeof(ReactiveMinds.Charity.FundRaise),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll().Any(x => x.FundRaise.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll().Count(x => x.FundRaise.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Поступление средств на кошелек -> Сбор",
                BaseEntity = typeof(ReactiveMinds.Charity.FundRaise),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Any(x => x.FundRaise.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Count(x => x.FundRaise.Id == id),
                GetDescriptionDependences = id => ""
            });

            #endregion

            #region Валюта
            References.Add(new EntityReference
            {
                ReferenceName = "Банк -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Bank>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Bank>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Перевод между кошельками -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletExchangeTransaction>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Клиника -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Clinic>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Clinic>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Оператор связи -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Telecom>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Telecom>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Транзакция -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Transaction>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Transaction>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Транзакция сбора -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.FundRaiseTransaction>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Вывод средств -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.OutTransaction>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.OutTransaction>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Кошелек -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Wallet>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Счет -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Account>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.Account>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Поступление средств на кошелек -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.WalletFundsRaiseTransaction>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            References.Add(new EntityReference
            {
                ReferenceName = "Платежная система -> Валюта",
                BaseEntity = typeof(ReactiveMinds.Charity.Currency),
                CheckAnyDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.PaymentSystem>().GetAll().Any(x => x.Currency.Id == id),
                GetCountDependences = id => Container.ResolveDomain<ReactiveMinds.Charity.PaymentSystem>().GetAll().Count(x => x.Currency.Id == id),
                GetDescriptionDependences = id => ""
            });

            #endregion
        }
    }
}