using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Bars.B4.Utils;
using Bars.B4.Modules.Ecm7.Framework;

[assembly: AssemblyTitle("ReactiveMinds.Charity")]
[assembly: AssemblyDescription("Благотворительность")]
[assembly: AssemblyCompany("ЗАО \"БАРС Груп\"")]
[assembly: AssemblyProduct("ReactiveMinds.Charity")]
[assembly: AssemblyCopyright("Copyright © 2017")]

[assembly: ComVisible(false)]

// формат версии ВерсияГенератора.Год.МесяцДень.КоличествоКомитов
[assembly: AssemblyVersion("2.2017.0418.4")]

// идентификатор модуля для миграций
[assembly: MigrationModule("ReactiveMinds.Charity")]