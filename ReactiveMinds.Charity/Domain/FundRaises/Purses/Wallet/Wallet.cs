namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Кошелек
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Кошелек")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("31aa67a1-13f8-487f-99e0-bcf381b185a0")]
    public class Wallet : ReactiveMinds.Charity.Account
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Wallet() : base()
        {
        }

        /// <summary>
        /// Сбор
        /// </summary>
        [Bars.B4.Utils.Display("Сбор")]
        public virtual ReactiveMinds.Charity.FundRaise FundRaise { get; set; }
        /// <summary>
        /// Базовый счет
        /// </summary>
        [Bars.B4.Utils.Display("Базовый счет")]
        public virtual ReactiveMinds.Charity.Account Account { get; set; }
        /// <summary>
        /// Баланс
        /// </summary>
        [Bars.B4.Utils.Display("Баланс")]
        public virtual System.Double Balance { get; set; }
    }

}