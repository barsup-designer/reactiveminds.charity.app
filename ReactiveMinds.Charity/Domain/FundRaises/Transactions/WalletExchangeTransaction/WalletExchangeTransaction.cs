namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Перевод между кошельками
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Перевод между кошельками")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("ac134829-421d-414f-8d44-11a60126580b")]
    public class WalletExchangeTransaction : ReactiveMinds.Charity.FundRaiseTransaction
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public WalletExchangeTransaction() : base()
        {
        }

    }

}