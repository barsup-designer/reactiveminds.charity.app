namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Вывод средств
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Вывод средств")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("59c302d2-6378-4f5d-94b5-96f71576a66a")]
    public class OutTransaction : ReactiveMinds.Charity.Transaction
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public OutTransaction() : base()
        {
        }

    }

}