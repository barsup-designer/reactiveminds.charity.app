namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Поступление средств на кошелек
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Поступление средств на кошелек")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("bc39c19f-0122-4abe-b723-1ab448b9f5cd")]
    public class WalletFundsRaiseTransaction : ReactiveMinds.Charity.FundRaiseTransaction
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public WalletFundsRaiseTransaction() : base()
        {
        }

    }

}