namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Транзакция сбора
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Транзакция сбора")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("4b804e50-e948-43fe-93b6-8678a20aa9db")]
    public class FundRaiseTransaction : ReactiveMinds.Charity.Transaction
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public FundRaiseTransaction() : base()
        {
        }

        /// <summary>
        /// Сбор
        /// </summary>
        [Bars.B4.Utils.Display("Сбор")]
        public virtual ReactiveMinds.Charity.FundRaise FundRaise { get; set; }
    }

}