namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Транзакция
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Транзакция")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("02d7696d-773e-47a0-a9bb-58b99117db0f")]
    public class Transaction : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Transaction() : base()
        {
        }

        /// <summary>
        /// Источник
        /// </summary>
        [Bars.B4.Utils.Display("Источник")]
        public virtual ReactiveMinds.Charity.Account Source { get; set; }
        /// <summary>
        /// Приемник
        /// </summary>
        [Bars.B4.Utils.Display("Приемник")]
        public virtual ReactiveMinds.Charity.Account Target { get; set; }
        /// <summary>
        /// Сумма в валюте
        /// </summary>
        [Bars.B4.Utils.Display("Сумма в валюте")]
        public virtual System.Double AmountInCurrency { get; set; }
        /// <summary>
        /// Валюта
        /// </summary>
        [Bars.B4.Utils.Display("Валюта")]
        public virtual ReactiveMinds.Charity.Currency Currency { get; set; }
        /// <summary>
        /// Сумма в рублях
        /// </summary>
        [Bars.B4.Utils.Display("Сумма в рублях")]
        public virtual System.Double AmountInRubles { get; set; }
        /// <summary>
        /// Комментарий
        /// </summary>
        [Bars.B4.Utils.Display("Комментарий")]
        public virtual System.String Description { get; set; }
        /// <summary>
        /// Дата
        /// </summary>
        [Bars.B4.Utils.Display("Дата")]
        public virtual System.DateTime TransactionDate { get; set; }
        /// <summary>
        /// Наименование (авто)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование (авто)")]
        public virtual System.String Name { get; set; }
    }

}