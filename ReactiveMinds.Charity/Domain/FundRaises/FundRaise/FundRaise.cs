namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Сбор
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Сбор")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("205ab413-e912-420c-b0c4-680b1b9ee4a0")]
    public class FundRaise : Bars.B4.DataAccess.PersistentObject
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public FundRaise() : base()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        public virtual System.String Name { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        [Bars.B4.Utils.Display("Описание")]
        public virtual System.String Description { get; set; }
    }

}