namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Валюта
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Валюта")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("f4489318-5ee6-4964-8e0a-8027db5b1023")]
    [Bars.B4.DataAccess.Attributes.DbTableInfo("CURRENCY")]
    public class Currency : Bars.B4.Modules.Nsi.Entities.NsiEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Currency() : base()
        {
        }

        /// <summary>
        /// Курс
        /// </summary>
        [Bars.B4.Utils.Display("Курс")]
        [Bars.B4.DataAccess.Attributes.DbColumnInfo(column: "exchangerate")]
        public virtual System.Double ExchangeRate { get; set; }
        /// <summary>
        /// Отображаемое имя
        /// </summary>
        [Bars.B4.Utils.Display("Отображаемое имя")]
        [Bars.B4.DataAccess.Attributes.DbColumnInfo(column: "displayname", Ignore = true)]
        public virtual System.String DisplayName { get; set; }
    }

}