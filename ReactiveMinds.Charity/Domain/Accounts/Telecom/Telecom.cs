namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Оператор связи
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Оператор связи")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("0e591910-5a84-43b1-9e55-449b7bc7b375")]
    public class Telecom : ReactiveMinds.Charity.Account
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Telecom() : base()
        {
        }

    }

}