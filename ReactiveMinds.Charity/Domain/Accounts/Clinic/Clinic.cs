namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Клиника
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Клиника")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("1d6eb087-b475-4989-a961-c18640018ebd")]
    public class Clinic : ReactiveMinds.Charity.Account
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Clinic() : base()
        {
        }

    }

}