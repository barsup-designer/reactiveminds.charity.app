namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Платежная система
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Платежная система")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("b656601b-2660-4617-adf6-4c02178426fd")]
    public class PaymentSystem : ReactiveMinds.Charity.Account
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public PaymentSystem() : base()
        {
        }

    }

}