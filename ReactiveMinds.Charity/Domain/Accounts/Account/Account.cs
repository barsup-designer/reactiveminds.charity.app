namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Счет
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Счет")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("32282a6c-78be-4b4b-a8fe-da88b5bd5b9b")]
    public class Account : Bars.B4.DataAccess.PersistentObject
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Account() : base()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        public virtual System.String Name { get; set; }
        /// <summary>
        /// Описание
        /// </summary>
        [Bars.B4.Utils.Display("Описание")]
        public virtual System.String Description { get; set; }
        /// <summary>
        /// Валюта счета
        /// </summary>
        [Bars.B4.Utils.Display("Валюта счета")]
        public virtual ReactiveMinds.Charity.Currency Currency { get; set; }
    }

}