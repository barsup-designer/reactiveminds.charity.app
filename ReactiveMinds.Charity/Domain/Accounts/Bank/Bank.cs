namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4.DataAccess;
    using System.Linq;

    /// <summary>
    /// Банк
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Банк")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("06382749-301e-46d6-aec4-cc42531c39de")]
    public class Bank : ReactiveMinds.Charity.Account
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Bank() : base()
        {
        }

    }

}