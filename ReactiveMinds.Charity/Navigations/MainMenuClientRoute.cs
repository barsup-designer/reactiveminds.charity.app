namespace ReactiveMinds.Charity
{
    using Bars.B4;

    public class MainMenuClientRoute : IClientRouteMapRegistrar
    {
        #region Implementation of IClientRouteMapRegistrar

        /// <summary>
        /// Метод регистрации роута в общей карте
        /// </summary>
        /// <param name="map"></param>
        public void RegisterRoutes(ClientRouteMap map)
        {
            map.AddRoute(new ClientRoute("CurrencyList/", "B4.controller.CurrencyList"));
            map.AddRoute(new ClientRoute("BankList/", "B4.controller.BankList"));
            map.AddRoute(new ClientRoute("ClinicList/", "B4.controller.ClinicList"));
            map.AddRoute(new ClientRoute("TelecomList/", "B4.controller.TelecomList"));
            map.AddRoute(new ClientRoute("PaymentSystemList/", "B4.controller.PaymentSystemList"));
            map.AddRoute(new ClientRoute("FundRaiseList/", "B4.controller.FundRaiseList"));
        }

        #endregion
    }
}