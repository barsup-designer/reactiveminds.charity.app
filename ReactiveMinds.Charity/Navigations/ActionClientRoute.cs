namespace ReactiveMinds.Charity
{
    using Bars.B4;

    public class ActionClientRoute : IClientRouteMapRegistrar
    {
        #region Implementation of IClientRouteMapRegistrar

        /// <summary>
        /// Метод регистрации роута в общей карте
        /// </summary>
        /// <param name="map"></param>
        public void RegisterRoutes(ClientRouteMap map)
        {
            map.AddRoute(new ClientRoute("FundRaiseList-Edit/{id}/", "B4.controller.FundRaiseList", "executeActionEdit"));
        }

        #endregion
    }
}