namespace ReactiveMinds.Charity
{
    using System;
    using Bars.B4;
    using Bars.B4.DataAccess;
    using System.Collections.Generic;
    using System.Web.Mvc;

    using System.Linq;

    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4.IoC;

    using Castle.Windsor;
    using Castle.MicroKernel.Registration;

    public class MainMenuNavigation : INavigationProvider
    {
        public IWindsorContainer Container { get; set; }

        #region Implementation of INavigationProvider

        /// <summary>
        /// Ключ меню
        /// </summary>
        public string Key
        {
            get { return MainNavigationInfo.MenuName; }
        }

        /// <summary>
        /// Описание меню
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// Метод инифиализации
        /// </summary>
        public void Init(MenuItem root)
        {
            MenuItem @ref0 = null;
            MenuItem @ref1 = null;
            MenuItem @ref2 = null;
            @ref0 = root.Add("Справочники");
            @ref1 = @ref0.Add("Справочники");
            @ref2 = @ref1.Add("Реестр валют", "CurrencyList");
            @ref2.Icon = "fi-money"; @ref1 = @ref0.Add("Счета");
            @ref2 = @ref1.Add("Банки", "BankList");
            @ref2.Icon = "fi-bank"; @ref2 = @ref1.Add("Клиники", "ClinicList");
            @ref2.Icon = "fm-medicine"; @ref2 = @ref1.Add("Операторы связи", "TelecomList");
            @ref2.Icon = "fi-phone-squared"; @ref2 = @ref1.Add("Платежные системы", "PaymentSystemList");
            @ref2.Icon = "fm-payment"; root.ReOrder(@ref0.Caption);
            @ref0 = root.Add("Сборы", "FundRaiseList");
            root.ReOrder(@ref0.Caption);

        }

        #endregion
    }
}